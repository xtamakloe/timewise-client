import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  static final String AUTH_TOKEN = 'token';

  static clearAuthToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(AUTH_TOKEN);
  }

  static Future<String> getAuthToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(AUTH_TOKEN) ?? null;
  }

  static saveAuthToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(AUTH_TOKEN, token);
  }
}
