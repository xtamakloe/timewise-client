import 'package:flutter/material.dart';
import 'package:time_wise_app/components/app_bar_title.dart';
import 'package:time_wise_app/screens/info/info_content.dart';

class InfoHomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: TimeWiseAppBar(
          title: 'Preparing for Rail Journeys',
          actions: [],
        ),
      ),
      body: InfoContent(),
    );
  }
}
