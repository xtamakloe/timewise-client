import 'package:flutter/material.dart';
import 'package:time_wise_app/screens/info/info_home_screen.dart';

class InfoView extends StatefulWidget {
  @override
  _InfoViewState createState() => _InfoViewState();
}

class _InfoViewState extends State<InfoView> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: null,
        child: Navigator(
          onGenerateRoute: (RouteSettings settings) {
            return MaterialPageRoute(
                settings: settings,
                builder: (BuildContext context) {
                  return InfoHomeScreen();
                });
          },
        ));
  }
}
