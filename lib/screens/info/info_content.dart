import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

class InfoContent extends StatefulWidget {
  @override
  _InfoContentState createState() => _InfoContentState();

  static TextStyle _textBodyStyle() {
    return TextStyle(
      fontSize: 14.0,
    );
  }

  static Widget _buildHeaderText(String text) {
    return Text(
      text,
      style: TextStyle(fontSize: 18.0, color: Colors.indigo[300]),
    );
  }

  static Widget _buildBodyText(String text) {
    return Text(
      text,
      style: _textBodyStyle(),
    );
  }

  static _buildListItem(String text) {
    return Column(
      children: [
        Row(
          children: [
            Flexible(
                flex: 1,
                child: Icon(
                  Icons.arrow_forward_ios_rounded,
                  color: Colors.indigo[300],
                )),
            SizedBox(
              width: 10.0,
            ),
            Flexible(
                flex: 4,
                child: Text(
                  text,
                  style: _textBodyStyle(),
                )),
          ],
        ),
        SizedBox(height: 10.0),
      ],
    );
  }
}

class _InfoContentState extends State<InfoContent> {
  PageController _pageController = PageController(
    initialPage: 0,
  );

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _pageController,
      children: [
        Page1(),
        Page2(),
        Page3(),
      ],
    );
  }
}

class Page1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(color: Colors.indigo[300], width: 2.0)),
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            InfoContent._buildHeaderText('Worthwhile Journeys'),
            SizedBox(height: 10.0),
            InfoContent._buildBodyText(
                'We engage in a wide variety of activities on the train when'
                ' we travel.\n\n'
                ' Some people like to work or study, while others like to'
                ' disconnect and relax with music or a tv show. \n\nOthers use'
                ' this time to work on personal errands or hobbies, and some'
                ' like to just zone out and observe the landscape. \n\nThese activities'
                ' make our travel time worthwhile.'),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.circle, size: 16.0, color: Colors.indigo[300]),
                  SizedBox(
                    width: 10.0,
                  ),
                  Icon(LineAwesomeIcons.circle,
                      size: 16.0, color: Colors.indigo[300]),
                  SizedBox(
                    width: 10.0,
                  ),
                  Icon(LineAwesomeIcons.circle,
                      size: 16.0, color: Colors.indigo[300])
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Page2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(color: Colors.indigo[300], width: 2.0)),
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            InfoContent._buildHeaderText('Importance of Preparation'),
            SizedBox(height: 10.0),
            InfoContent._buildBodyText(
                'Research shows that taking a few minutes to prepare for how you'
                ' spend your time on a journey leads to a worthwhile use of your'
                ' time.'),
            SizedBox(height: 10.0),
            InfoContent._buildBodyText(
                'Travellers who plan ahead are usually happier with'
                ' how they spent their time during the journey and'
                ' have a better experience overall.'),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Icon(LineAwesomeIcons.circle,
                      size: 16.0, color: Colors.indigo[300]),
                  SizedBox(
                    width: 10.0,
                  ),
                  Icon(Icons.circle, size: 16.0, color: Colors.indigo[300]),
                  SizedBox(
                    width: 10.0,
                  ),
                  Icon(LineAwesomeIcons.circle,
                      size: 16.0, color: Colors.indigo[300])
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Page3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(color: Colors.indigo[300], width: 2.0)),
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            InfoContent._buildHeaderText('TimeWise App'),
            SizedBox(height: 10.0),
            InfoContent._buildBodyText(
                'This app is designed to support you as you prepare for your journey.\n\n'
                ' Before your journey, use the app to:'),

            SizedBox(height: 10.0),
            InfoContent._buildListItem(
                'Find out how busy your specific train usually is'
                ' (and your chances of getting a seat) and think of how'
                ' that impacts how you want to spend your time.'),
            InfoContent._buildListItem('Be more organised by creating a'
                ' checklist of activities you’d like to do to prepare.'
                ' You will periodically receive friendly reminders before the trip.'),
            // InfoContent._buildListItem(
            //     'Keeps track of your past trip experiences so'
            //         ' you can better plan for your future journeys'),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Icon(LineAwesomeIcons.circle,
                      size: 16.0, color: Colors.indigo[300]),
                  SizedBox(
                    width: 10.0,
                  ),
                  Icon(LineAwesomeIcons.circle,
                      size: 16.0, color: Colors.indigo[300]),
                  SizedBox(
                    width: 10.0,
                  ),
                  Icon(Icons.circle, size: 16.0, color: Colors.indigo[300])
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
