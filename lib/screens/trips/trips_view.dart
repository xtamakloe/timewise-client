import 'package:flutter/material.dart';
import 'package:time_wise_app/screens/trips/trips_list_screen.dart';
import 'package:time_wise_app/screens/trips/trips_overview_screen.dart';
import 'package:time_wise_app/screens/wizard/trip_planner_screen.dart';

class TripsView extends StatefulWidget {
  const TripsView({
    Key key,
    String startRoute,
  })  : startRoute = startRoute,
        super(key: key);

  final String startRoute;

  @override
  _TripsViewState createState() => _TripsViewState();
}

class _TripsViewState extends State<TripsView> {
  @override
  Widget build(BuildContext context) {
    // TOOD: implement for android back button
    // return WillPopScope(child: Navigator(), onWillPop: () sync {});

    return WillPopScope(
      child: Navigator(
        onGenerateRoute: (RouteSettings settings) {
          final arguments = settings.arguments;
          return MaterialPageRoute(
            settings: settings,
            // ignore: missing_return
            builder: (BuildContext context) {
              switch (settings.name) {
                case '/':
                  return TripsOverviewScreen();
                case '/tripsHome':
                  return TripsOverviewScreen();
                case '/tripsList':
                  return TripsListScreen(arguments);
                case '/tripPlanner':
                  return TripPlannerScreen();
              }
            },
          );
        },
      ),
      onWillPop: null,
    );
  }
}
