import 'dart:async';

import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:time_wise_app/components/app_bar_title.dart';
import 'package:time_wise_app/components/screen_section.dart';
import 'package:time_wise_app/components/trip_list.dart';
import 'package:time_wise_app/components/tw_snackbar.dart';
import 'package:time_wise_app/models/trip.dart';
import 'package:time_wise_app/provider/app_state.dart';
import 'package:time_wise_app/provider/trips_state.dart';
import 'package:time_wise_app/screens/wizard/trip_planner_screen.dart';

class TripsOverviewScreen extends StatefulWidget {
  @override
  _TripsOverviewScreenState createState() => _TripsOverviewScreenState();
}

class _TripsOverviewScreenState extends State<TripsOverviewScreen> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  Future<void> _refreshFromServer() async {
    String authToken =
        Provider.of<AppState>(context, listen: false).user.authToken;
    Provider.of<TripsState>(context, listen: false)
        .loadTripsFromServer(authToken);
    TWSnackBar.showAlert(context, 'success', 'Trips Refreshed');
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<TripsState>(
      builder: (context, tripsState, _) {
        List<Trip> trips = tripsState.tripsList ?? [];

        trips.sort((a, b) => b.departsAt.compareTo(a.departsAt));
        List<Trip> upcoming = trips.where((trip) => trip.isUpcoming()).toList();
        List<Trip> inProgress =
            trips.where((trip) => trip.isInProgress()).toList();
        List<Trip> completed =
            trips.where((trip) => trip.isCompleted()).toList();

        List<ScreenSectionData> sectionsData = <ScreenSectionData>[
          if (trips != null && trips.isEmpty)
            ScreenSectionData(
              sectionTitle: '',
              sectionAction: SectionAction(),
              sectionContent: Container(
                  height: MediaQuery.of(context).size.height * .7,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(LineAwesomeIcons.train , size: 60.0),
                        SizedBox(height: 20.0),
                        Text('No trips added.',
                            style: TextStyle(fontSize: 16.0)),
                      ],
                    ),
                  )),
            ),
          if (inProgress.isNotEmpty)
            ScreenSectionData(
                sectionTitle: 'IN-PROGRESS',
                sectionAction: SectionAction(),
                sectionContent: TripList(
                  trips: inProgress,
                )),
          if (upcoming.isNotEmpty)
            ScreenSectionData(
                sectionTitle: 'UPCOMING',
                sectionAction: SectionAction(
                    title: 'Show more',
                    route: '/tripsList',
                    routeArguments: {'tripType': 'upcoming'}),
                sectionContent: TripList(
                  trips: upcoming.take(3).toList(),
                )),
          if (completed.isNotEmpty)
            ScreenSectionData(
                sectionTitle: 'PREVIOUS',
                sectionAction: SectionAction(
                    title: 'Show more',
                    route: '/tripsList',
                    routeArguments: {'tripType': 'completed'}),
                sectionContent: TripList(
                  trips: completed.take(3).toList(),
                )),
        ];

        return Scaffold(
          backgroundColor: Colors.grey[100],
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(60.0),
              child: TimeWiseAppBar(title: 'Trips', actions: [
                // action button
              ])),
          body: RefreshIndicator(
            onRefresh: _refreshFromServer,
            color: Colors.indigo[300],
            child: ListView.builder(
                itemCount: sectionsData.length,
                itemBuilder: (context, index) {
                  return ScreenSection(
                    sectionTitle: sectionsData[index].sectionTitle,
                    sectionAction: sectionsData[index].sectionAction,
                    sectionContent: sectionsData[index].sectionContent,
                  );
                }),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.miniEndFloat,
          floatingActionButton: _buildFloatingActionButton(context),
        );
      },
    );

    /*
    final UpdateTripsBloc bloc = BlocProvider.of<UpdateTripsBloc>(context);

    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(60.0),
          child: TimeWiseAppBar(title: 'Trips', actions: [
            // action button
          ])),

      body: RefreshIndicator(
          onRefresh: _refreshFromServer,
          color: Colors.indigo[300],
          child: StreamBuilder<List<Trip>>(
            stream: bloc.tripsStream, // listening to trip stream
            initialData: [],
            builder: (BuildContext context,
                AsyncSnapshot<List<Trip>> snapshotTrips) {

              List<Trip> trips =
              snapshotTrips?.data != null ? snapshotTrips.data : [];

              trips.sort((a, b) => b.departsAt.compareTo(a.departsAt));
              List<Trip> upcoming =
              trips.where((trip) => trip.isUpcoming()).toList();
              List<Trip> inProgress =
              trips.where((trip) => trip.isInProgress()).toList();
              List<Trip> completed =
              trips.where((trip) => trip.isCompleted()).toList();

              List<ScreenSectionData> sectionsData = <ScreenSectionData>[
                if (trips != null && trips.isEmpty)
                  ScreenSectionData(
                    sectionTitle: '',
                    sectionAction: SectionAction(),
                    sectionContent: Container(
                        height: MediaQuery.of(context).size.height * .7,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.search,
                                size: 60.0,
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              Text(
                                'No trips added.',
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ],
                          ),
                        )),
                  ),
                if (inProgress.isNotEmpty)
                  ScreenSectionData(
                      sectionTitle: 'IN-PROGRESS',
                      sectionAction: SectionAction(),
                      sectionContent: TripList(
                        trips: inProgress,
                        onTripChanged: _notifyTripsOverviewScreen,
                      )),
                if (upcoming.isNotEmpty)
                  ScreenSectionData(
                      sectionTitle: 'UPCOMING',
                      sectionAction: SectionAction(
                          title: 'Show more',
                          route: '/tripsList',
                          routeArguments: {
                            'tripType': 'upcoming',
                            'onTripChanged': _notifyTripsOverviewScreen,
                          }),
                      sectionContent: TripList(
                        trips: upcoming.take(3).toList(),
                        onTripChanged: _notifyTripsOverviewScreen,
                      )),
                if (completed.isNotEmpty)
                  ScreenSectionData(
                      sectionTitle: 'PREVIOUS',
                      sectionAction: SectionAction(
                          title: 'Show more',
                          route: '/tripsList',
                          routeArguments: {
                            'tripType': 'completed',
                            'onTripChanged': _notifyTripsOverviewScreen,
                          }),
                      sectionContent: TripList(
                        trips: completed.take(3).toList(),
                        onTripChanged: _notifyTripsOverviewScreen,
                      )),
              ];

              return ListView.builder(
                  itemCount: sectionsData.length,
                  itemBuilder: (context, index) {
                    return ScreenSection(
                      sectionTitle: sectionsData[index].sectionTitle,
                      sectionAction: sectionsData[index].sectionAction,
                      sectionContent: sectionsData[index].sectionContent,
                    );
                  });
            },
          )),
      // child: ListView.builder(
      //     itemCount: sectionsData.length,
      //     itemBuilder: (context, index) {
      //       return ScreenSection(
      //         sectionTitle: sectionsData[index].sectionTitle,
      //         sectionAction: sectionsData[index].sectionAction,
      //         sectionContent: sectionsData[index].sectionContent,
      //       );
      //     })),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,
      floatingActionButton: _buildFloatingActionButton(context),
    );*/
  }

  FloatingActionButton _buildFloatingActionButton(BuildContext context) {
    return FloatingActionButton(
      // isExtended: true,
      child: Icon(LineAwesomeIcons.calendar_plus, size: 30.0),
      backgroundColor: Colors.indigo[300],
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TripPlannerScreen()),
        );
      },
    );
  }
}
