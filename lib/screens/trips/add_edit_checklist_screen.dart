import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_wise_app/components/tw_flatbutton.dart';
import 'package:time_wise_app/components/tw_snackbar.dart';
import 'package:time_wise_app/models/checklist_item.dart';
import 'package:time_wise_app/models/trip.dart';
import 'package:time_wise_app/provider/app_state.dart';
import 'package:time_wise_app/provider/trips_state.dart';
import 'package:time_wise_app/services/checklist_service.dart';

class AddEditChecklistScreen extends StatefulWidget {
  final Trip trip;
  final int checklistItemIndex; // add or edit

  AddEditChecklistScreen({
    Key key,
    @required this.checklistItemIndex,
    @required this.trip,
  }) : super(key: key);

  @override
  _AddEditChecklistScreenState createState() => _AddEditChecklistScreenState();
}

class _AddEditChecklistScreenState extends State<AddEditChecklistScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _formController;

  @override
  Widget build(BuildContext context) {
    String initialValue = widget.checklistItemIndex != null
        ? widget.trip.checklistItems[widget.checklistItemIndex].text
        : null;

    _formController = TextEditingController(text: initialValue);

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 20.0),
          Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            InkWell(
              child: Icon(
                Icons.clear,
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            SizedBox(
              width: 50.0,
            ),
            Text(
                widget.checklistItemIndex == null
                    ? 'Add item to checklist'
                    : 'Editing item',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0)),
          ]),
          Expanded(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          decoration: InputDecoration(
                            hintText: 'Charge mobile phone...',
                          ),
                          controller: _formController,
                          validator: (value) =>
                              value.isEmpty ? 'Item can\'t be empty' : null,
                        ),
                        SizedBox(
                          height: 40.0,
                        ),
                        TWFlatButton(
                          inverted: false,
                          context: context,
                          buttonText: 'SAVE',
                          onPressed: () async {
                            final form = _formKey.currentState;
                            if (form.validate()) {
                              if (widget.checklistItemIndex != null) {
                                await _updateChecklistItem();
                              } else {
                                await _createChecklistItem();
                              }

                              Provider.of<TripsState>(context, listen: false)
                                  .updateLocalTrip(widget.trip);

                              // return to details screen
                              _showDialog(context);
                              // close modal screen and return updated widget
                              Navigator.pop(context, widget.trip);
                            }
                          },
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _updateChecklistItem() async {
    ChecklistItem item = widget.trip.checklistItems[widget.checklistItemIndex];
    item.text = _formController.text;

    await item.update(context, widget.trip).then((responseItem) {
      // replace local checklist item with updated one from server
      widget.trip.checklistItems.replaceRange(widget.checklistItemIndex,
          widget.checklistItemIndex + 1, [responseItem]);
    });
  }

  _createChecklistItem() async {
    // Create new list item on server
    await ChecklistService()
        .createChecklistItem(
            Provider.of<AppState>(context, listen: false)
                .user
                .authToken,
            widget.trip,
            ChecklistItemFormData(text: _formController.text))
        .then((responseItem) {
      // save list item to local widget trip
      widget.trip.checklistItems.add(responseItem);
    });
  }

  _showDialog(BuildContext context) {
    TWSnackBar.showAlert(context, 'success', 'Saved!');
  }
}
