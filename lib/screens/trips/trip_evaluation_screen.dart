import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:time_wise_app/components/eval_question.dart';
import 'package:time_wise_app/components/tw_flatbutton.dart';
import 'package:time_wise_app/components/tw_radiobutton_question.dart';
import 'package:time_wise_app/models/evaluation_report.dart';
import 'package:time_wise_app/models/trip.dart';
import 'package:time_wise_app/provider/trips_state.dart';

class TripEvaluationScreen extends StatefulWidget {
  final Trip trip;

  TripEvaluationScreen({Key key, @required this.trip});

  @override
  _TripEvaluationScreenState createState() => _TripEvaluationScreenState();
}

class _TripEvaluationScreenState extends State<TripEvaluationScreen> {
  int _selectedTimeWorth = -1;
  int _selectedPrepLevel = -1;
  int _selectedTripExperience = -1;

  List<bool> _isSelectedTripExperience = List.generate(5, (_) => false);
  static List<String> allActivities = [
    'eat_drink',
    'communicate',
    'read',
    'write',
    'browse',
    'video',
    'audio',
    'games',
    'sleep',
    'socialise',
    'window_gaze',
    'care',
    'other'
  ];
  List<bool> _isSelectedActivities =
      List.generate(allActivities.length, (_) => false);

  List<String> _selectedFactors = [];

  List<bool> isSelectedWithTimeWise = [false, false];
  List<bool> isSelectedWithoutTimeWise = [false, false];
  List<bool> isSelectedAbilityToDo = [false, false];
  List<bool> isSelectedService = [false, false];
  List<bool> isSelectedInfo = [false, false];
  List<bool> isSelectedCrowding = [false, false];
  List<bool> isSelectedSeat = [false, false];
  List<bool> isSelectedPower = [false, false];
  List<bool> isSelectedTemp = [false, false];
  List<bool> isSelectedNoise = [false, false];
  List<bool> isSelectedCleanliness = [false, false];
  List<bool> isSelectedPrivacy = [false, false];
  List<bool> isSelectedSecurity = [false, false];
  List<bool> isSelectedPassengers = [false, false];
  List<bool> isSelectedStation = [false, false];
  List<bool> isSelectedOther = [false, false];

  String notes = '';

  // Saved data
  Trip _setTripAttributes(Trip trip) {
    trip.status = 'completed';

    EvaluationReport evaluationReport = trip.getEvaluationReport();
    ActivityDataReport activityReport =
        evaluationReport.getActivityDataReport();
    ExperienceFactorsReport experienceFactorsReport =
        evaluationReport.getExperienceFactorsReport();

    // 1
    evaluationReport.tripExperienceRating = _selectedTripExperience;

    // 2 - trip activities => activityDataReport
    List<String> selectedActivities = [];
    for (int i = 0; i < _isSelectedActivities.length; i++) {
      if (_isSelectedActivities[i]) {
        selectedActivities.add(allActivities[i]);
      }
    }
    activityReport.activityData =
        selectedActivities.map((i) => i.toString()).join(",");

    // 3 - time use rating => worthwhile vs wasted
    evaluationReport.timeUseRating = _selectedTimeWorth;

    // 4 - experience factors => experienceFactorsReport
    experienceFactorsReport.experienceFactors = [];

    _selectedFactors.forEach((element) {
      List<String> split = element.split('-');
      experienceFactorsReport.experienceFactors.add(ExperienceFactor(
          reportId: experienceFactorsReport.id,
          code: split[0],
          rating: int.parse(split[1])));
    });

    // 5 - post trip preparation rating
    evaluationReport.postTripPrepRating = _selectedPrepLevel;

    // 6 - notes
    evaluationReport.tripNotes = notes;

    // trip.rating =
    //     (_isSelectedTripExperience.indexWhere((flag) => flag) + 1).toString();

    return trip;
  }

  @override
  Widget build(BuildContext context) {
    Trip _trip = widget.trip;

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 20.0),
          Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            InkWell(
              child: Icon(
                Icons.clear,
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            SizedBox(
              width: 50.0,
            ),
          ]),
          Expanded(
              child: Container(
                  child:
                      SingleChildScrollView(child: _tripEvalContent(_trip)))),
        ],
      ),
    );
  }

  Widget _newRatingScale() {
    // print(_isSelectedTripExperience);
    // print(_isSelectedTripExperience.indexWhere((element) => element == true));

    return ToggleButtons(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: Icon(LineAwesomeIcons.loudly_crying_face,
              size: 40.0, color: Colors.red[700]),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: Icon(LineAwesomeIcons.frowning_face,
              size: 40.0, color: Colors.red[300]),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: Icon(LineAwesomeIcons.neutral_face,
              size: 40.0, color: Colors.amber),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: Icon(LineAwesomeIcons.smiling_face,
              size: 40.0, color: Colors.green[300]),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: Icon(LineAwesomeIcons.grinning_face_with_smiling_eyes,
              size: 40.0, color: Colors.green[700]),
        ),
      ],
      // selectedColor: Colors.white,
      fillColor: Colors.white,
      selectedBorderColor: Colors.indigo[300],
      borderColor: Colors.white,
      borderRadius: BorderRadius.circular(5.0),
      isSelected: _isSelectedTripExperience,
      onPressed: (int index) {
        // set trip experience variable
        _selectedTripExperience = index + 1;

        setState(() {
          for (int buttonIndex = 0;
              buttonIndex < _isSelectedTripExperience.length;
              buttonIndex++) {
            if (buttonIndex == index)
              _isSelectedTripExperience[buttonIndex] = true;
            else
              _isSelectedTripExperience[buttonIndex] = false;
          }
        });
      },
    );
  }

  Widget _activityPicker() {
//    print(_isSelectedActivities);
//    print(_isSelectedActivities.indexWhere((element) => element == true));

    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(color: Colors.indigo[300], width: 2.0)),
        height: 170.0,
        child: Scrollbar(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: ToggleButtons(
              children: <Widget>[
                _activityTile('Eat or drink', LineAwesomeIcons.hamburger),
                _activityTile('Make phone calls or send messages',
                    LineAwesomeIcons.mobile_phone),
                _activityTile('Read', LineAwesomeIcons.book_open),
                _activityTile('Write or edit documents', LineAwesomeIcons.edit),
                _activityTile(
                    'Browse the Internet (social media, travel info, etc.)',
                    LineAwesomeIcons.globe),
                _activityTile('Watch videos', LineAwesomeIcons.film),
                _activityTile('Listen to music, podcasts, audio books or radio',
                    LineAwesomeIcons.headphones),
                _activityTile('Play games', LineAwesomeIcons.gamepad),
                _activityTile('Take a nap', LineAwesomeIcons.bed),
                _activityTile(
                    'Window gaze or people watch', LineAwesomeIcons.binoculars),
                _activityTile(
                    'Talk to other passengers', Icons.connect_without_contact),
                _activityTile('Care for other passengers',
                    LineAwesomeIcons.universal_access),
                _activityTile('Other', LineAwesomeIcons.question_circle),
              ],
              borderWidth: 5.0,
              color: Colors.black,
              selectedColor: Colors.black,
              fillColor: Colors.grey[100],
              borderColor: Colors.transparent,
              selectedBorderColor: Colors.transparent,
              isSelected: _isSelectedActivities,
              onPressed: (int index) {
                setState(() {
                  _isSelectedActivities[index] = !_isSelectedActivities[index];
                });
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _tripEvalContent(Trip trip) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        _divider(),
        _divider(),
        EvalQuestion('How was your trip?'),
        _newRatingScale(),
        _divider(),
        EvalQuestion('What did you do?'),
        _activityPicker(),
        _divider(),
        TWRadioButtonQuestion(
            context: context,
            selectedValue: _selectedTimeWorth,
            onChanged: (value) => setState(() {
                  _selectedTimeWorth = value;
                }),
            minLabel: 'All time was wasted',
            maxLabel: 'All time was worthwhile',
            questionText:
                'Was your journey time \nworthwhile/productive or wasted?'),
        _divider(),
        _customEvalQuestion(),
        _experienceFactorRater(),
        _divider(),
        TWRadioButtonQuestion(
            context: context,
            selectedValue: _selectedPrepLevel,
            onChanged: (value) => setState(() {
                  _selectedPrepLevel = value;
                }),
            minLabel: 'I could have done more',
            maxLabel: 'I was well prepared',
            questionText: 'Looking back, how adequate was your preparation?'),
        _divider(),
        _tripNotes(),
        _divider(),
        TWFlatButton(
          inverted: false,
          context: context,
          buttonText: 'END TRIP',
          onPressed: () {
            Trip updatedTrip = _setTripAttributes(trip);

            // update on server
            updatedTrip.update(context).then((trip) {
              Provider.of<TripsState>(context, listen: false)
                  .updateLocalTrip(trip);

              Scaffold.of(context).showSnackBar(SnackBar(
                  backgroundColor: Colors.green,
                  content: Text('Trip ended',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold))));

              // Close screen and return to details screen. Updating handled there
              Navigator.pop(context, trip);
            });
          },
        ),
      ],
    );
  }

  Widget _customEvalQuestion() {
    TextStyle style = TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold);

    return Container(
      child: Column(
        children: [
          Row(
            children: [],
          ),
          Text('What affected how you spent ', style: style),
          Text(' your time on this journey ', style: style),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('positively (', style: style),
              Icon(
                LineAwesomeIcons.plus_circle,
                color: Colors.green,
              ),
              Text(') and negatively (', style: style),
              Icon(LineAwesomeIcons.minus_circle, color: Colors.red),
              Text(')?', style: style)
            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5.0),
            child: Text(
              'Please rate only those that affected you',
              style: TextStyle(fontSize: 14.0),
            ),
          ),
        ],
      ),
    );
  }

  Widget _experienceFactorRater() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(color: Colors.indigo[300], width: 2.0)),
        height: 300.0,
        child: Scrollbar(
          child: SingleChildScrollView(
            child: Column(children: [
              _experienceFactorRating('Preparation with TimeWise',
                  LineAwesomeIcons.app_store, isSelectedWithTimeWise, 0),
              _experienceFactorRating('Preparation without TimeWise',
                  LineAwesomeIcons.app_store, isSelectedWithoutTimeWise, 1),
              _experienceFactorRating('Ability to do what you wanted',
                  LineAwesomeIcons.raised_fist, isSelectedAbilityToDo, 2),
              _experienceFactorRating('Train reliability & punctuality',
                  LineAwesomeIcons.stopwatch, isSelectedService, 3),
              _experienceFactorRating('Information availability & accuracy',
                  LineAwesomeIcons.bullhorn, isSelectedInfo, 4),
              _experienceFactorRating(
                  'Crowding', LineAwesomeIcons.users, isSelectedCrowding, 5),
              _experienceFactorRating('Seat availability',
                  Icons.airline_seat_recline_normal, isSelectedSeat, 6),
              _experienceFactorRating(
                  'Power supply', LineAwesomeIcons.plug, isSelectedPower, 7),
              _experienceFactorRating('Coach temperature',
                  LineAwesomeIcons.thermometer_1_2_full, isSelectedTemp, 8),
              _experienceFactorRating('Coach noise levels',
                  LineAwesomeIcons.deaf, isSelectedNoise, 9),
              _experienceFactorRating('Coach cleanliness',
                  LineAwesomeIcons.broom, isSelectedCleanliness, 10),
              _experienceFactorRating('Privacy', LineAwesomeIcons.user_secret,
                  isSelectedPrivacy, 11),
              _experienceFactorRating('Security',
                  LineAwesomeIcons.alternate_shield, isSelectedSecurity, 12),
              _experienceFactorRating('Other passengers',
                  LineAwesomeIcons.user_friends, isSelectedPassengers, 13),
              _experienceFactorRating(
                  'Station', LineAwesomeIcons.archway, isSelectedStation, 14),
              _experienceFactorRating('Other', LineAwesomeIcons.question_circle,
                  isSelectedOther, 15),
            ]),
          ),
        ),
      ),
    );
  }

  static List<String> allFactors = [
    'timewise',
    'prep',
    'freedom',
    'reliability',
    'information',
    'crowding',
    'seating',
    'power',
    'temperature',
    'noise',
    'cleanliness',
    'privacy',
    'security',
    'passengers',
    'station',
    'other'
  ];

  Widget _experienceFactorRating(String label, IconData icon,
      List<bool> isSelectedFlag, int factorZeroIndex) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(flex: 1, child: Icon(icon, size: 30.0)),
          Expanded(
              flex: 4,
              child: Text(
                label,
                style: TextStyle(fontSize: 16.0),
              )),
          Expanded(
            flex: 3,
            child: ToggleButtons(
//              color: Colors.indigo[300],
              fillColor: Colors.grey[100],
              // Colors.indigo[300],
              borderColor: Colors.grey[100],
              // Colors.indigo[300],
//              borderWidth: 2.0,
//              selectedColor: Colors.white,
//              selectedBorderColor: Colors.grey, //Colors.indigo[300],
              borderRadius: BorderRadius.circular(5.0),
              children: [
//                Icon(LineAwesomeIcons.thumbs_down),
//                Icon(LineAwesomeIcons.thumbs_up)
                Icon(LineAwesomeIcons.minus_circle, color: Colors.red),
                Icon(
                  LineAwesomeIcons.plus_circle,
                  color: Colors.green,
                ),
              ],
              onPressed: (int index) {
                // update field variable
                String currentFactorString = allFactors[factorZeroIndex];
                String addedFactor = _selectedFactors.firstWhere(
                    (element) => element.startsWith(currentFactorString),
                    orElse: () => null);
                // // remove factor if already added
                if (addedFactor != null)
                  _selectedFactors.removeWhere(
                      (element) => element.startsWith(currentFactorString));
                // add factor to list
                _selectedFactors.add('${allFactors[factorZeroIndex]}-$index');

                // Update UI
                setState(() {
                  for (int buttonIndex = 0;
                      buttonIndex < isSelectedFlag.length;
                      buttonIndex++) {
                    if (buttonIndex == index) {
                      isSelectedFlag[buttonIndex] = true;
                    } else {
                      isSelectedFlag[buttonIndex] = false;
                    }
                  } // for loop
                });
              },
              isSelected: isSelectedFlag,
            ),
          ),
        ],
      ),
    );
  }

  Widget _activityTile(String label, IconData icon) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
      margin: const EdgeInsets.symmetric(horizontal: 5.0),
      width: 110.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            icon,
            size: 50.0,
          ),
          SizedBox(height: 20.0),
          Flexible(
              fit: FlexFit.tight,
              child: Text(
                label,
                textAlign: TextAlign.center,
              )),
        ],
      ),
    );
  }

  Widget _divider() {
    return Column(
      children: [
        SizedBox(height: 10.0),
//        Divider(),
        SizedBox(height: 10.0),
      ],
    );
  }

  Widget _tripNotes() {
    return Container(
      child: Column(children: [
        EvalQuestion('What preparation could you have done to make'
            ' this trip better, if any?'),
        SizedBox(
          height: 10.0,
        ),
        TextField(
            decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.edit,
                color: Colors.grey,
              ),
            ),
            onChanged: (text) => {this.notes = text}),
      ]),
    );
  }
}
