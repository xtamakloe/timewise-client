import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_wise_app/components/tw_flatbutton.dart';
import 'package:time_wise_app/components/tw_radiobutton_question.dart';
import 'package:time_wise_app/components/tw_snackbar.dart';
import 'package:time_wise_app/models/evaluation_report.dart';
import 'package:time_wise_app/models/trip.dart';
import 'package:time_wise_app/provider/trips_state.dart';

class TripStartScreen extends StatefulWidget {
  final Trip trip;

  TripStartScreen({Key key, @required this.trip});

  @override
  _TripStartScreenState createState() => _TripStartScreenState();
}

class _TripStartScreenState extends State<TripStartScreen> {
  Trip trip;
  int _selectedPrepLevel = -1;

  @override
  Widget build(BuildContext context) {
    this.trip = widget.trip;

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 20.0),
          Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            InkWell(
              child: Icon(
                Icons.clear,
              ),
              onTap: () {
                Navigator.pop(context);
              },
            )
          ]),
          Expanded(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TWRadioButtonQuestion(
                      context: context,
                      selectedValue: _selectedPrepLevel,
                      onChanged: (value) => setState(() {
                            _selectedPrepLevel = value;
                          }),
                      minLabel: 'Not very prepared',
                      maxLabel: 'Very well prepared',
                      questionText: 'How well are you prepared for this trip?'),
                  SizedBox(
                    height: 40.0,
                  ),
                  TWFlatButton(
                      inverted: false,
                      context: context,
                      buttonText: 'START TRIP',
                      onPressed: () {
                        if (_selectedPrepLevel == -1) {
                          return TWSnackBar.showAlert(
                              context, "error", 'Please select a rating');
                        }

                        // save evaluation data
                        EvaluationReport report = trip.getEvaluationReport();
                        report.preTripPrepRating = _selectedPrepLevel;

                        // update trip status
                        trip.status = 'in-progress';

                        // update on server
                        trip.update(context).then((trip) {
                          // update locally
                          Provider.of<TripsState>(context, listen: false)
                              .updateLocalTrip(trip);

                          TWSnackBar.showAlert(
                              context, 'success', 'Trip started');

                          // Close screen and return to details screen. Updating handled there
                          Navigator.pop(context, trip);
                        });
                      }),
                ],
              ),
            ),
          ),
//          ScreenSection(
//            sectionTitle: '',
//            sectionAction: SectionAction(),
//            sectionContent: Expanded(
//              child: Padding(
//                padding: const EdgeInsets.all(10.0),
//              ),
//            ),
//          ),
        ],
      ),
    );
  }
}
