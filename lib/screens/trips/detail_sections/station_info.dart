import 'package:flutter/material.dart';

class StationInfoContent extends StatefulWidget {
  @override
  _StationInfoContentState createState() => _StationInfoContentState();
}

class _StationInfoContentState extends State<StationInfoContent> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: Row(
                children: [], // names of stations goes here
              ),
            ),
          ),
          Divider(),
          IndexedStack(
            children: [], // graphs of station populations goes here
          ),
        ],
      ),
    );
  }
}
