import 'package:flutter/material.dart';
import 'package:time_wise_app/models/trip.dart';

class TrainInfoContent extends StatelessWidget {
  TrainInfoContent({
    Key key,
    @required this.trip,
  });

  final Trip trip;

  @override
  Widget build(BuildContext context) {
    List<String> features = this.trip.featureList.split(',');

    return Container(
      padding: EdgeInsets.all(16.0),
      height: MediaQuery.of(context).size.height * .7,
      child: Column(
        children: [
          SizedBox(height: 20.0),
          Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            InkWell(
              child: Icon(
                Icons.clear,
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ]),
          SizedBox(height: 20.0,),
          Text(
            'This service is by ${trip.operatorName}'
            ' and provides the following:',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16.0),
          ),
          SizedBox(height: 10.0),
          Expanded(
            child: ListView.builder(
                itemCount: features.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    leading: Icon(Icons.circle, color: Colors.indigo[300],),
                    title: Text('${features[index]}'),
                  );
                }),
          ),
        ],
      ),
    );
  }
}
