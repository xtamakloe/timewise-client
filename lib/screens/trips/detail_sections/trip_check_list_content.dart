import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:time_wise_app/components/tw_snackbar.dart';
import 'package:time_wise_app/models/checklist_item.dart';
import 'package:time_wise_app/models/trip.dart';
import 'package:time_wise_app/screens/trips/add_edit_checklist_screen.dart';

class TripChecklistContent extends StatefulWidget {
  const TripChecklistContent({
    Key key,
    @required this.trip,
  }) : super(key: key);

  final Trip trip;

  @override
  _TripChecklistContentState createState() => _TripChecklistContentState();
}

class _TripChecklistContentState extends State<TripChecklistContent> {
  List<bool> inputs;
  List<String> labels;
  int itemCount = 0;

  _itemChange(bool val, int index) {
    ChecklistItem item = widget.trip.checklistItems[index];
    // if (item != null) {
    item.complete = val;

    item.update(context, widget.trip).then((responseItem) {
      widget.trip.checklistItems[index].complete = responseItem.complete;
      setState(() => inputs[index] = val);
      if (val == true) _showAlert(context, 'Completed!', 'success');
    });
    // }
  }

  @override
  Widget build(BuildContext context) {
    inputs = [];
    labels = [];

    itemCount = widget.trip.checklistItems.length;

    for (var i = 0; i < itemCount; i++) {
      inputs.add(widget.trip.checklistItems[i].complete);
      labels.add(widget.trip.checklistItems[i].text);
    }

    return Container(
      width: MediaQuery.of(context).size.width,
      height: 400.0,
      child: widget.trip.checklistItems.isEmpty
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    LineAwesomeIcons.tasks,
                    size: 50.0,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * .6,
                    child: Text(
                      'Add to-do items here and be reminded before the trip!',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18.0),
                    ),
                  ),
                ],
              ),
            )
          : ListView.builder(
              itemCount: itemCount,
              itemBuilder: (BuildContext context, int index) {
                return Slidable(
                    key: ValueKey(index),
                    actionPane: SlidableDrawerActionPane(),
                    secondaryActions: [
                      IconSlideAction(
                        caption: 'Edit',
                        color: Colors.grey.shade300,
                        icon: Icons.edit,
                        closeOnTap: true,
                        onTap: () {
                          showCupertinoModalBottomSheet(
                              expand: true,
                              backgroundColor: Colors.white,
                              context: context,
                              builder: (context, scrollController) {
                                return AddEditChecklistScreen(
                                  trip: widget.trip,
                                  checklistItemIndex: index,
                                );
                              }).then((updatedTrip) {
                            if (updatedTrip != null) setState(() {});
                          });
                        },
                      ),
                      IconSlideAction(
                        caption: 'Delete',
                        color: Colors.red.shade300,
                        icon: Icons.delete,
                        closeOnTap: true,
                        onTap: () async {
                          // remove from server
                          await widget.trip.checklistItems[index]
                              .delete(context, widget.trip)
                              .then((success) {
                            // remove locally
                            if (success != null && success) {
                              widget.trip.checklistItems.removeAt(index);
                              // refresh screen
                              setState(() {});
                              _showAlert(context, 'Item deleted!', 'success');
                            } else
                              _showAlert(
                                  context,
                                  'Error occurred while removing item. '
                                      'Please check your Internet connection',
                                  'error');
                          });
                        },
                      ),
                    ],
                    dismissal:
                        SlidableDismissal(child: SlidableDrawerDismissal()),
                    child: CheckboxListTile(
                        checkColor: Colors.indigo,
                        activeColor: Colors.white,
                        value: inputs[index],
                        title: Text(labels[index]),
                        controlAffinity: ListTileControlAffinity.leading,
                        onChanged: (bool val) {
                          _itemChange(val, index);
                        }));
              }),
    );
  }

  void _showAlert(BuildContext context, String message, String type) {
    TWSnackBar.showAlert(context, type, message);
  }
}
