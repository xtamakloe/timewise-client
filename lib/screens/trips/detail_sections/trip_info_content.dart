import 'package:flutter/material.dart';
import 'package:time_wise_app/components/detail_item.dart';
import 'package:time_wise_app/components/tw_flatbutton.dart';
import 'package:time_wise_app/main.dart';
import 'package:time_wise_app/models/trip.dart';
import 'package:time_wise_app/screens/trips/trip_evaluation_screen.dart';
import 'package:time_wise_app/screens/trips/trip_start_screen.dart';
import 'package:time_wise_app/util/notification_helper.dart';

class TripInfoContent extends StatefulWidget {
  TripInfoContent({Key key, @required this.trip}) : super(key: key);

  final Trip trip;

  @override
  _TripInfoContentState createState() => _TripInfoContentState();
}

class _TripInfoContentState extends State<TripInfoContent> {
  @override
  Widget build(BuildContext context) {
    Trip _trip = widget.trip;

    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // TODO: v2 need a banner or some indicator to show trip status
          //  (en-route, upcoming etc)
          if (_trip.isInProgress())
            Container(
              height: 20.0,
              width: MediaQuery.of(context).size.width,
              child: Text(
                'TRIP IN-PROGRESS',
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green, fontSize: 16.0),
              ),
            ),
          if (_trip.isCompleted())
            Container(
              height: 20.0,
              width: MediaQuery.of(context).size.width,
              child: Text(
                'TRIP COMPLETED',
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red, fontSize: 16.0),
              ),
            ),
          // InkWell(
          //   child: FlatButton(
          //     child: Text('Hello'),
          //   ),
          //   onTap: () {
          //     // // testing
          //     scheduleNotification(
          //         flutterLocalNotificationsPlugin,
          //         '3',
          //         'Title here',
          //         "You're almost there, 10 minutes to go! "
          //             "Please click here to rate your journey.",
          //         DateTime.now().add(Duration(seconds: 3)),
          //         widget.trip.id.toString());
          //   },
          // ),

          SizedBox(height: 10.0),
// origin - destination
          Column(
            children: [
              SizedBox(
                height: 20.0,
              ),
              Text(
                _trip.originStationName,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10.0),
                child: Image(
                    height: 12.0,
                    width: 12.0,
                    fit: BoxFit.contain,
                    image: AssetImage('assets/images/arrow-down.png')),
              ),
              Text(
                _trip.destinationStationName,
                textAlign: TextAlign.right,
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
            ],
          ),
          SizedBox(height: 15.0),
// description
          Text(_trip.purposeDescription),
          SizedBox(height: 15.0),
// schedule
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    _trip.date,
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
              VerticalDivider(
                thickness: 2.0,
                color: Colors.grey.shade100,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    _trip.schedule,
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: 40,
          ),
// other details
//          Divider(thickness: 1.0, height: 40.0,),
          DetailItemText(
            detailIcon: Icons.timer,
            detailLabel: _trip.isCompleted() ? 'Duration' : 'Expected Duration',
            detailValue: _trip.duration,
          ),
          SizedBox(height: 40.0),
//          DetailItemText(
//            detailIcon: _trip.isBusiness()
//                ? LineAwesomeIcons.briefcase
//                : LineAwesomeIcons.theater_masks,
//            detailLabel: 'Trip Type',
//            detailValue: _trip.type.capitalize(),
//          ),
          /*
          DetailItemWidget(
            detailIcon: LineAwesomeIcons.cloud_with_sun,
            detailLabel: 'Weather',
            detailWidget: Text(
              '14 ' + String.fromCharCode($deg) + 'C',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
          ),
          // TODO: hide this if there are no events!
          DetailItemWidget(
            detailIcon: LineAwesomeIcons.map_pin,
            detailLabel: 'Events',
            detailWidget: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconTile(
                  onTap: () {
                    showMaterialModalBottomSheet(
                        context: context,
                        builder: (context, scrollController) {
                          return Container(
                            padding: EdgeInsets.all(30.0),
                            child: Text('Bank holiday on this day'),
                          );
                        });
                  },
                  icon: LineAwesomeIcons.calendar_with_day_focus,
                ),
//                SizedBox(
//                  width: 5.0,
//                ),
//                IconTile(
//                  onTap: () {
//                    showMaterialModalBottomSheet(
//                        context: context,
//                        builder: (context, scrollController) {
//                          return Container(
//                            padding: EdgeInsets.all(30.0),
//                            child: Text('Champions League match on this day'),
//                          );
//                        });
//                  },
//                  icon: LineAwesomeIcons.futbol_1,
//                ),
              ],
            ),
          ),
*/
// start/stop trip button
//          if (!_trip.isCompleted()) _statusSection(context, _trip),
        ],
      ),
    );
  }

  Column _statusSection(BuildContext context, Trip trip) {
    return Column(
      children: [
        Text(
          _tripStatusMessage(trip),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 16.0,
          ),
        ),
        Container(
            width: double.infinity,
            margin: EdgeInsets.all(20),
            child: _statusButton(context, trip)),
      ],
    );
  }

  String _tripStatusMessage(Trip trip) {
    if (trip.isUpcoming()) return 'This trip has not yet started';

    if (trip.isCompleted()) return 'This trip has ended';

    return 'This trip is currently in progress';
  }

  TWFlatButton _statusButton(BuildContext context, Trip trip) {
    return TWFlatButton(
        inverted: false,
        context: context,
        buttonText: trip.isUpcoming() ? 'START TRIP' : 'END TRIP',
        onPressed: () {
          if (trip.isUpcoming()) {
            _navigateAndDisplaySelection(
              context,
              trip,
              TripStartScreen(trip: trip),
            );
          } else if (trip.isInProgress()) {
            _navigateAndDisplaySelection(
              context,
              trip,
              TripEvaluationScreen(trip: trip),
            );
          }
        });
  }

  _navigateAndDisplaySelection(
      BuildContext context, Trip trip, Widget route) async {
    // Navigator.push returns a Future that completes after calling
    // Navigator.pop on the Selection Screen.
    final result = await Navigator.push(
      context,
      // Create the SelectionScreen in the next step.
      MaterialPageRoute(
        builder: (context) => route,
      ),
    );
    if (result != null) {
      setState(() => trip = result);
    }
  }
}
