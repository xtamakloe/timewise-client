import 'package:flutter/material.dart';
import 'package:time_wise_app/components/app_bar_title.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TrainInfoWebView extends StatefulWidget {
  final String url;
  final String title;

  @override
  _TrainInfoWebViewState createState() => _TrainInfoWebViewState();

  TrainInfoWebView({@required this.url, @required this.title});
}

class _TrainInfoWebViewState extends State<TrainInfoWebView> {
  // String _url =
  //     'https://www.eastmidlandsrailway.co.uk/travel-information/seat-finder';

  WebViewController _webViewController;
  bool _pageLoading;

  @override
  void initState() {
    super.initState();

    _pageLoading = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0),
            child: TimeWiseAppBar(
              title: widget.title,
              actions: [
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                  child: IconButton(
                    icon: Icon(Icons.refresh, size: 25.0),
                    onPressed: () {
                      _webViewController.loadUrl(widget.url);
                    },
                  ),
                ),
              ],
            )),
        body: Stack(
          children: [
            WebView(
              onPageStarted: (url) {
                _showProgressIndicator();
              },
              onPageFinished: (url) {
                _hideProgressIndicator();
              },
              initialUrl: widget.url,
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController controller) {
                _webViewController = controller;
              },
            ),
            _pageLoading
                ? Container(
                    height: MediaQuery.of(context).size.height,
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.indigo[300]),
                      ),
                    ),
                  )
                : Stack(),
          ],
        ));
  }

  void _showProgressIndicator() {
    setState(() {
      _pageLoading = true;
    });
  }

  void _hideProgressIndicator() {
    setState(() {
      _pageLoading = false;
    });
  }
}
