import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:time_wise_app/components/icon_tile.dart';
import 'package:time_wise_app/components/leg_heat_map.dart';
import 'package:time_wise_app/models/rating_cell.dart';
import 'package:time_wise_app/models/trip.dart';
import 'dart:math' as math;

import 'package:time_wise_app/services/trip_service.dart';

class CoverageInfoContent extends StatefulWidget {
  CoverageInfoContent({
    Key key,
    @required this.trip,
  }) : super(key: key);

  final Trip trip;

  @override
  _CoverageInfoContentState createState() => _CoverageInfoContentState();
}

class _CoverageInfoContentState extends State<CoverageInfoContent> {
  int _activeTileIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10.0),
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            _optionTiles(),
            Divider(),
            IndexedStack(
              index: _activeTileIndex,
              children: [
                _routeHeatMap2('Phone'),
                _routeHeatMap2('Data'),
                _routeHeatMap2('WiFi'),
                _routeHeatMap2('Occupancy')
              ],
            ),
            Divider(),
            _heatMapKey()
          ],
        ));
  }

  _optionTiles() {
    return Container(
      height: 65.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _optionIconTile('Phone', Icons.phone_in_talk, _activeTileIndex == 0),
          _optionIconTile(
              'Data', LineAwesomeIcons.signal, _activeTileIndex == 1),
          _optionIconTile('WiFi', Icons.wifi, _activeTileIndex == 2),
          _optionIconTile('Occupancy', Icons.people, _activeTileIndex == 3),
        ],
      ),
    );
  }

  _optionIconTile(String option, IconData icon, bool active) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(3.0),
      child: Container(
        padding: const EdgeInsets.all(10.0),
        color: active ? Colors.grey[300] : Colors.white,
//      decoration: active
//          ? BoxDecoration(
//              border: Border(
//              bottom: BorderSide(width: 3.0, color: Colors.indigo),
//            ))
//          : null,
        child: InkWell(
          child: Column(
            children: [
              Image(
                  width: 30.0,
                  height: 30.0,
                  fit: BoxFit.contain,
                  image: AssetImage(
                    'assets/images/trips/coverage/$option.png',
                  )),
              Expanded(child: Text(option)),
            ],
          ),
          onTap: () {
            setState(() {
              //TODO: show data for option selected
              switch (option) {
                case 'Phone':
                  _activeTileIndex = 0;
                  break;
                case 'Data':
                  _activeTileIndex = 1;
                  break;
                case 'WiFi':
                  _activeTileIndex = 2;
                  break;
                case 'Occupancy':
                  _activeTileIndex = 3;
                  break;
              }
            });
          },
        ),
        /*
        child: IconTile(
          onTap: () {
            setState(() {
              //TODO: show data for option selected
              switch (option) {
                case 'phone':
                  _activeTileIndex = 0;
                  break;
                case 'data':
                  _activeTileIndex = 1;
                  break;
                case 'wifi':
                  _activeTileIndex = 2;
                  break;
              }
            });
          },
          icon: icon,
          iconColor: active ? Colors.indigo : null,
        ),
        */
      ),
    );
  }

  _routeHeatMap(String type) {
    return Container(
        height: 250.0,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: Row(
              children: widget.trip.tripLegs
                  .map((leg) => LegHeatMap(leg, type))
                  .toList(),
            ),
          ),
        ));
  }

  _routeHeatMap2(String type) {
    return Container(
        height: 250.0,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: Row(
                children: widget.trip.ratingCells
                    .map((cell) => _ratingCellColumn(cell))
                    .toList(),
              )),
        ));
  }

  Widget _ratingCellColumn(RatingCell cell) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 2.0),
      child: Column(
        children: [
          _emptyCell(),
          _ratingCell(cell.rating),
          _labelCell(cell.stop_Time),
          RotatedBox(
            quarterTurns: 1,
            child: Container(
                alignment: Alignment.centerLeft,
                width: 130.0,
                child: Container(
                    padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                    child: Text(cell.stopName == null ? '' : cell.stopName))),
          ),
//        Expanded(
////          child: _newLabelCell('London St Pancras International'),
//          child: _newLabelCell('Wellingborough'),
//        )
        ],
      ),
    );
  }

  _emptyCell() {
    return Container(
      height: 20.0,
      width: 50.0,
      color: Colors.transparent,
    );
  }

  _labelCell(String labelText) {
    return Container(
      height: 20.0,
      width: 50.0,
      child: Text(
        labelText == null ? '' : labelText,
        overflow: TextOverflow.visible,
        textAlign: TextAlign.center,
      ),
    );
  }

  _ratingCell(String rating) {
    Color color;
    switch (rating) {
      case '0':
        {
//        color = Colors.red[300];
          color = Colors.grey[200];
        }
        break;
      case '1':
        {
          color = Colors.red[300];
        }
        break;
      case '2':
        {
          color = Colors.red[100];
        }
        break;
      case '3':
        {
          color = Colors.amber[200];
        }
        break;
      case '4':
        {
          color = Colors.green[100];
        }
        break;
      case '5':
        {
          color = Colors.green[300];
        }
        break;
    }
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      height: 20.0,
      width: 50.0,
      color: color,
    );
  }

  _heatMapKey() {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
      width: MediaQuery.of(context).size.width,
      child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _keyColumn(Colors.grey[200], 'No Data'),
            SizedBox(
              width: 5,
            ),
            _keyColumn(Colors.red[300], 'Bad'), // Low
            SizedBox(
              width: 5,
            ),
            _keyColumn(Colors.red[100], 'Poor'), // Moderate
            SizedBox(
              width: 5,
            ),
            _keyColumn(Colors.amber[200], 'Average'), // Good
            SizedBox(
              width: 5,
            ),
            _keyColumn(Colors.green[100], 'Good'),
            SizedBox(
              width: 5,
            ),
            _keyColumn(Colors.green[300], 'Excellent'),
          ]),
    );
  }

  _keyColumn(Color color, String label) {
    return Container(
      height: 70,
      child: Column(
        children: [_keyColor(color), _keyLabel(label)],
      ),
    );
  }

  _keyColor(Color color) {
    return Container(
      height: 20,
      width: 50,
      color: color,
    );
  }

  _keyLabel(String labelText) {
    return Container(
      height: 20,
      width: 50,
      margin: const EdgeInsets.fromLTRB(0, 5, 0, 0),
      child: Text(
        labelText,
        style: TextStyle(
          fontSize: 10.0,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}
