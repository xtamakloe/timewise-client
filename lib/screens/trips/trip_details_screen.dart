import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';
import 'package:time_wise_app/components/app_bar_title.dart';
import 'package:time_wise_app/components/screen_section.dart';
import 'package:time_wise_app/components/tw_snackbar.dart';
import 'package:time_wise_app/models/trip.dart';
import 'package:time_wise_app/provider/trips_state.dart';
import 'package:time_wise_app/screens/trips/add_edit_checklist_screen.dart';
import 'package:time_wise_app/screens/trips/detail_sections/train_info_content.dart';
import 'package:time_wise_app/screens/trips/detail_sections/train_loading_web_view.dart';
import 'package:time_wise_app/screens/trips/detail_sections/trip_check_list_content.dart';
import 'package:time_wise_app/screens/trips/detail_sections/trip_info_content.dart';
import 'package:time_wise_app/screens/trips/trip_evaluation_screen.dart';
import 'package:time_wise_app/screens/trips/trip_start_screen.dart';

class TripDetailsScreen extends StatefulWidget {
  final Trip trip;

  TripDetailsScreen({Key key, @required this.trip}) : super(key: key);

  @override
  _TripDetailsScreenState createState() => _TripDetailsScreenState();
}

class _TripDetailsScreenState extends State<TripDetailsScreen> {
  Trip _trip;

  // local version of onTripChanged => this one should refresh TripDetailsScreen
  // and then call whatever onTripChanged was passed into this widget as well
  _onTripChanged(Trip updatedTrip) {
    _trip = updatedTrip;
  }

  @override
  void initState() {
    super.initState();
    // set up trip variable when screen is initially built
    _trip = widget.trip;
  }

  @override
  Widget build(BuildContext context) {
    List<ScreenSectionData> sectionsData = <ScreenSectionData>[];

    sectionsData.add(ScreenSectionData(
      sectionTitle: '',
      sectionAction: SectionAction(),
      sectionContent: TripInfoContent(trip: _trip),
    ));

    // if (_trip.featureList != null)
    //   sectionsData.add(ScreenSectionData(
    //     sectionTitle: 'TRAIN INFO',
    //     sectionAction: SectionAction(
    //       title: 'Seat Finder',
    //       route: '/tripSeatFinder',
    //       routeArguments: null,
    //     ),
    //     sectionContent: TrainInfoContent(trip: _trip),
    //   ));

//    sectionsData.add(ScreenSectionData(
//      sectionTitle: 'STATION INFO',
//      sectionAction: SectionAction(),
//      sectionContent: CoverageInfoContent(trip: _trip),
//    ));

    sectionsData.add(ScreenSectionData(
      sectionTitle: 'PREPARATION CHECKLIST',
      sectionAction: SectionAction(
        title: 'Add item',
        screen: AddEditChecklistScreen(trip: _trip),
        type: 'modal',
        onModalClosed: (updatedTrip) {
          // if not the cancel button => really need to return something more sensible?!
          if (updatedTrip != null) {
            setState(() {
              // updates both detail screen and trip list screen
              _onTripChanged(updatedTrip);
            });
          }
        },
      ),
      sectionContent: TripChecklistContent(trip: _trip),
    ));

    // sectionsData.add(ScreenSectionData(
    //   sectionTitle: 'AVAILABLE SERVICES',
    //   sectionAction: SectionAction(),
    //   sectionContent: ServicesInfoContent(trip: _trip),
    // ));

    // sectionsData.add(ScreenSectionData(
    //   sectionTitle: 'SERVICE TRACKER',
    //   sectionAction: SectionAction(),
    //   sectionContent: CoverageInfoContent(trip: _trip),
    // ));

    return Scaffold(
        backgroundColor: Colors.grey[100],
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0),
            child: TimeWiseAppBar(
              title: 'Trip • Details',
              actions: [
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                  child: IconButton(
                      icon: Icon(Icons.more_horiz, size: 30.0),
                      onPressed: () async {
                        await showCupertinoModalBottomSheet(
                            expand: false,
                            context: context,
                            backgroundColor: Colors.transparent,
                            builder: (context, scrollController) {
                              return TripDetailsScreenMenu(
                                  trip: _trip,
                                  scrollController: scrollController);
                            }).then((operationResult) {
                          // all menu items need to return a TRIP(updated) if they
                          // want the TripDetailsScreen to be updated
                          // THIS IS THE ONLY PLACE UPDATING TripDetailsScreen
                          // SHOULD BE DONE.
                          if (operationResult != null)
                            setState(() => _onTripChanged(operationResult));
                        });
                      }),
                ),
              ],
            )),
        body: ListView.builder(
            itemCount: sectionsData.length,
            itemBuilder: (context, index) {
              return _buildSection(context, sectionsData[index]);
            }));
  }

  _buildSection(BuildContext context, ScreenSectionData sectionData) {
    return ScreenSection(
      sectionTitle: sectionData.sectionTitle,
      sectionAction: sectionData.sectionAction,
      sectionContent: sectionData.sectionContent,
    );
  }
}

class TripDetailsScreenMenu extends StatelessWidget {
  final Trip trip;
  final ScrollController scrollController;

  TripDetailsScreenMenu({Key key, this.scrollController, this.trip})
      : super(key: key);

  // if (_trip.featureList != null)
  //   sectionsData.add(ScreenSectionData(
  //     sectionTitle: 'TRAIN INFO',
  //     sectionAction: SectionAction(
  //       title: 'Seat Finder',
  //       route: '/tripSeatFinder',
  //       routeArguments: null,
  //     ),
  //     sectionContent: TrainInfoContent(trip: _trip),
  //   ));

  @override
  Widget build(BuildContext context) {
    return Material(
        child: SafeArea(
      top: false,
      child: Container(
        color: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Column(
              children: [
                if (!trip.isCompleted()) _showTrainInformationMenu(context),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            if (trip.isUpcoming())
              Column(
                children: [
                  ListTile(
                    title: Text(
                      'Begin Trip',
                      style: TextStyle(fontSize: 18.0),
                    ),
                    leading: Icon(
                      Icons.play_arrow,
                      color: Colors.green[300],
                    ),
                    onTap: () async {
                      showCupertinoModalBottomSheet(
                          context: context,
                          builder: (context, scrollController) {
                            return TripStartScreen(trip: trip);
                          }).then((trip) {
                        var result =
                            (trip != null && trip.status == 'in-progress')
                                ? trip
                                : null;

                        // UPDATING TripDetailsScreen is handled in the
                        // onPressed callback of the action menu button.
                        // So what gets sent back here is whether the operation
                        // (start trip) was successful or not => in the form of a trip or null
                        Navigator.pop(context, result);
                      });
                    },
                  ),
                ],
              ),
            if (trip.isInProgress())
              Column(
                children: [
                  ListTile(
                    title: Text(
                      'End Trip',
                      style: TextStyle(fontSize: 18.0),
                    ),
                    leading: Icon(
                      LineAwesomeIcons.flag_checkered,
                      color: Colors.indigo[300],
                    ),
                    onTap: () async {
                      showCupertinoModalBottomSheet(
//                    expand: true,
                          context: context,
                          builder: (context, scrollController) {
                            return TripEvaluationScreen(trip: trip);
                          }).then((trip) {
                        var result =
                            (trip != null && trip.status == 'completed')
                                ? trip
                                : null;

                        // UPDATING TripDetailsScreen is handled in the
                        // onPressed callback of the action menu button.
                        // So what gets sent back here is whether the operation
                        // (start trip) was successful or not => in the form of a trip or null
                        Navigator.pop(context, result);
                      });
                    },
                  ),
                ],
              ),
            ListTile(
                title: Text(
                  'Remove Trip',
                  style: TextStyle(fontSize: 18.0),
                ),
                leading: Icon(
                  Icons.delete,
                  color: Colors.red[300],
                ),
                onTap: () {
                  // remove from server
                  trip.delete(context).then((success) {
                    if (success != null && success) {
                      Provider.of<TripsState>(context, listen: false)
                          .removeLocalTrip(trip);

                      TWSnackBar.showAlert(context, 'success', 'Trip removed');

                      Navigator.popUntil(context, ModalRoute.withName('/'));
                    } else
                      Scaffold.of(context).showSnackBar(SnackBar(
                          backgroundColor: Colors.red,
                          content: Text(
                              'An error occurred while deleting this trip. '
                              'Please check your Internet connection.')));
                  });
                }),
          ],
        ),
      ),
    ));
  }

  _showTrainInformationMenu(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'Travel Information',
                  style: TextStyle(fontSize: 16.0, color: Colors.indigo[300]),
                  textAlign: TextAlign.left,
                ),
              ],
            ),
          ),
          if (trip.featureList != null)
            ListTile(
              title: Text(
                'Train Facilities',
                style: TextStyle(fontSize: 18.0),
              ),
              leading: Icon(
                Icons.train,
                color: Colors.indigo[300],
              ),
              onTap: () {
                showCupertinoModalBottomSheet(
                    context: context,
                    builder: (context, scrollController) {
                      return TrainInfoContent(trip: trip);
                    });
              },
            ),
          ListTile(
            title: Text(
              'Seat Finder',
              style: TextStyle(fontSize: 18.0),
            ),
            leading: Icon(
              Icons.airline_seat_recline_normal,
              color: Colors.indigo[300],
            ),
            onTap: () {
              // Navigator.popAndPushNamed(context, '/tripSeatFinder');
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => TrainInfoWebView(
                            title: 'SEAT FINDER',
                            url:
                                'https://www.eastmidlandsrailway.co.uk/travel-information/seat-finder',
                          )));
            },
          ),
          ListTile(
            title: Text(
              'Train Status Alerts',
              style: TextStyle(fontSize: 18.0),
            ),
            leading: Icon(
              Icons.railway_alert,
              color: Colors.indigo[300],
            ),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => TrainInfoWebView(
                            title: 'STATUS ALERTS',
                            url:
                                'https://www.eastmidlandsrailway.co.uk/timetables-updates/train-status-alerts',
                          )));
            },
          ),
          ListTile(
            title: Text(
              'COVID-19 Travel Information',
              style: TextStyle(fontSize: 18.0),
            ),
            leading: Icon(
              LineAwesomeIcons.medical_briefcase,
              color: Colors.indigo[300],
            ),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => TrainInfoWebView(
                        title: 'COVID-19',
                        url: 'https://www.eastmidlandsrailway.co.uk/covid-19'),
                  ));
            },
          ),
          Divider(),
        ],
      ),
    );
  }
}
