import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_wise_app/components/app_bar_title.dart';
import 'package:time_wise_app/components/screen_section.dart';
import 'package:time_wise_app/components/trip_list.dart';
import 'package:time_wise_app/models/trip.dart';
import 'package:time_wise_app/provider/trips_state.dart';

class TripsListScreen extends StatelessWidget {
  final Map arguments;

  const TripsListScreen(this.arguments, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<TripsState>(
      builder: (context, tripsState, _) {
        List<Trip> _trips = tripsState.tripsList;
        String _title = 'Trips';

        switch (arguments['tripType']) {
          case 'upcoming':
            {
              _title = 'Trips • Upcoming';
              _trips = _trips.where((trip) => trip.isUpcoming()).toList();
            }
            break;
          case 'in-progress':
            {
              _title = 'Trips • In-Progress';
              _trips = _trips.where((trip) => trip.isInProgress()).toList();
            }
            break;
          case 'completed':
            {
              _title = 'Trips • Previous';
              _trips = _trips.where((trip) => trip.isCompleted()).toList();
            }
            break;
          default:
        }

        return Scaffold(
          backgroundColor: Colors.grey[100],
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0),
            child: TimeWiseAppBar(title: _title, actions: []),
          ),
          body: ListView(
            children: [
              ScreenSection(
                  sectionTitle: '',
                  sectionAction: SectionAction(),
                  sectionContent: TripList(trips: _trips)),
            ],
          ),
        );
      },
    );
  }
}
