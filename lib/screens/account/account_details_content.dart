import 'package:feedback/feedback.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:time_wise_app/components/tw_flatbutton.dart';
import 'package:time_wise_app/components/tw_snackbar.dart';
import 'package:time_wise_app/provider/app_state.dart';
import 'package:time_wise_app/provider/auth_state.dart';
import 'package:time_wise_app/provider/trips_state.dart';
import 'package:time_wise_app/screens/trips/detail_sections/train_loading_web_view.dart';

class AccountDetailsContent extends StatefulWidget {
  @override
  _AccountDetailsContentState createState() => _AccountDetailsContentState();
}

class _AccountDetailsContentState extends State<AccountDetailsContent> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(10.0),
      child: Column(children: [
        Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // Container(
              //   width: 150.0,
              //   height: 150.0,
              //   child: CircleAvatar(
              //     backgroundImage: AssetImage('assets/images/user.png'),
              //     backgroundColor: Colors.indigo[300],
              //   ),
              // ),
              // SizedBox(height: 20.0),
//              Text('JON',
//                  style:
//                  TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
//               Text(StateContainer.of(context).getAppState().user.name,
//                   style:
//                       TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
//               SizedBox(height: 5.0),
              // Text(StateContainer.of(context).getAppState().user.email,
              //   style: TextStyle(
              //     fontSize: 16.0,
              //   ),
              // ),
              // SizedBox(height: 30.0),
              // Container(
              //   width: 200.0,
              //   child: TWFlatButton(
              //       inverted: false,
              //       context: context,
              //       buttonText: 'EDIT ACCOUNT',
              //       onPressed: () {
              //         Navigator.pushNamed(context, '/accountEdit');
              //       }),
              // ),
              SizedBox(height: 20.0),
              Container(
                width: 200.0,
                child: TWFlatButton(
                    inverted: false,
                    context: context,
                    buttonText: 'Refresh',
                    onPressed: () {
                      var authToken =
                          Provider.of<AppState>(context, listen: false)
                              .user
                              .authToken;

                      Provider.of<TripsState>(context, listen: false)
                          .loadTripsFromServer(authToken)
                          .then((value) => TWSnackBar.showAlert(
                              context, "success", 'Done!'));
                    }),
              ),
              SizedBox(height: 20.0),
              Container(
                width: 200.0,
                child: TWFlatButton(
                    inverted: false,
                    context: context,
                    buttonText: 'Feedback',
                    onPressed: () {
                      BetterFeedback.of(context).show();
                    }),
              ),
              SizedBox(height: 20.0),
              Container(
                width: 200.0,
                child: TWFlatButton(
                    inverted: false,
                    context: context,
                    buttonText: 'User Guide',
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TrainInfoWebView(
                                title: 'User Guide',
                                url:
                                'https://timewise-study.herokuapp.com/guide',
                              )));
                    }),
              ),
              SizedBox(height: 20.0),
              Container(
                width: 200.0,
                child: TWFlatButton(
                    inverted: false,
                    context: context,
                    buttonText: 'Logout',
                    onPressed: () {
                      Provider.of<AuthState>(context, listen: false).signOut();

                      Provider.of<AppState>(context, listen: false).logout();
                    }),
              ),
              SizedBox(height: 30.0),
              FutureBuilder(
                  future: getVersionNumber(),
                  builder:
                      (BuildContext context, AsyncSnapshot<String> snapshot) =>
                          Text(
                            snapshot.hasData ? snapshot.data : "Loading ...",
                          ) // The widget using the data
                  ),
            ],
          ),
        )
      ]),
    );
  }

  Future<String> getVersionNumber() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return 'Version ${packageInfo.version} Build ${packageInfo.buildNumber}';

    // Other data you can get:
    //
    // 	String appName = packageInfo.appName;
    // 	String packageName = packageInfo.packageName;
    //	String buildNumber = packageInfo.buildNumber;
  }
}
