import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_wise_app/provider/auth_state.dart';
import 'package:time_wise_app/provider/app_state.dart';
import 'package:time_wise_app/provider/trips_state.dart';
import 'package:time_wise_app/components/login_button.dart';
import 'package:time_wise_app/components/login_logo.dart';
import 'package:time_wise_app/components/login_text_field.dart';

class LoginPage extends StatefulWidget {
  final String authErrorMsg;

  const LoginPage({Key key, this.authErrorMsg}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();

  String _email;
  String _password;

  @override
  Widget build(BuildContext context) {
    Widget showAuthError(String message) {
      return Center(
        child: Text(message,
            style: TextStyle(fontSize: 16.0, color: Colors.white)),
      );
    }

    Widget showForm() {
      return Container(
        padding: const EdgeInsets.all(15.0),
        color: Theme.of(context).primaryColor,
        width: double.infinity,
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 150.0, 0, 50.0),
                child: LoginLogo(),
              ),
              if (widget.authErrorMsg != null)
                showAuthError(widget.authErrorMsg),
              SizedBox(height: 10.0),
              LoginTextField(
                hintText: 'Username',
                autofocus: false,
                obscureText: false,
                maxLines: 1,
                keyboardType: TextInputType.emailAddress,
                validator: (value) =>
                    value.isEmpty ? 'Username can\'t be empty' : null,
                onSaved: (value) => _email = value.trim(),
              ),
              SizedBox(height: 10.0),
              LoginTextField(
                hintText: 'Password',
                obscureText: true,
                autofocus: false,
                maxLines: 1,
                validator: (value) =>
                    value.isEmpty ? 'Password can\'t be empty' : null,
                onSaved: (value) => _password = value.trim(),
              ),
              SizedBox(
                height: 20.0,
              ),
              LoginButton(
                labelText: 'Log in',
                onPressed: validateAndSubmit,
              ),
            ],
          ),
        ),
      );
    }

    // TODO: remove BlocProvider
    return Scaffold(
        body: Stack(
      children: [
        showForm(),
      ],
    ));
  }

  // Check if form is valid before performing login op
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  // Perform login or signup
  void validateAndSubmit() async {
    if (validateAndSave()) {
      try {
        AuthState authState = Provider.of<AuthState>(context, listen: false);
        AppState appState =
            Provider.of<AppState>(context, listen: false);
        TripsState tripsState = Provider.of<TripsState>(context, listen: false);

        authState.signIn(_email, _password).then((user) =>
            user != null ? appState.initAppState(user, tripsState) : null);
      } catch (e) {
        print('Error: $e');
        setState(() => _formKey.currentState.reset());
      }
    }
  }
}
