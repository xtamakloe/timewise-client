import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_wise_app/models/trip.dart';
import 'package:time_wise_app/provider/app_state.dart';
import 'package:time_wise_app/provider/auth_state.dart';
import 'package:time_wise_app/provider/trips_state.dart';
import 'package:time_wise_app/screens/home_page.dart';
import 'package:time_wise_app/screens/login_page.dart';
import 'package:time_wise_app/screens/trips/trip_details_screen.dart';
import 'package:time_wise_app/util/notification_helper.dart';

class StartPage extends StatefulWidget {
  @override
  _StartPageState createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // incomplete
    selectNotificationSubject.stream.listen((event) {
      if (Provider.of<AppState>(context, listen: false).user?.authToken !=
          null) {
        List<Trip> trips =
            Provider.of<TripsState>(context, listen: false).tripsList;

        Trip trip = trips.firstWhere((trip) => trip.id.toString() == event);
        if (trip != null)
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => TripDetailsScreen(trip: trip)),
            ModalRoute.withName('/'),
          );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AuthState>(
      builder: (context, authState, _) {
        // print('status ${authState.status}');
        switch (authState.status) {
          case Status.Uninitialized:
            authState.checkPreviousLogin(context);
            return LoginPage();
          case Status.Authenticating:
            return LoadingScreen();
          case Status.NotAuthenticated:
            return LoginPage(
              authErrorMsg: authState.authError,
            );
          case Status.Authenticated:
            return HomePage();
          default:
            return Container();
        }
      },
    );
  }
}

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text('Initializing...'),
    );
  }
}

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.indigo[300],
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
        ),
      ),
    );
  }
}
