import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:time_wise_app/screens/account/account_view.dart';
import 'package:time_wise_app/screens/info/info_view.dart';
import 'package:time_wise_app/screens/trips/trips_view.dart';

class HomePage extends StatefulWidget {
  // final User user;

  // HomePage({Key key, @required this.user}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    _buildBottomNavigationBar() {
      return Theme(
        data: Theme.of(context).copyWith(
          // sets the background color of the `BottomNavigationBar`
          canvasColor: Colors.white,
// // sets the active color of the `BottomNavigationBar` if `Brightness` is light
//            primaryColor: Colors.red,
//            texttheme: theme.of(context)
//                .texttheme
//                .copywith(caption: new textstyle(color: colors.yellow))
        ),
        // sets the inactive color of the `BottomNavigationBar`
        child: Container(
          decoration: BoxDecoration(
              border: Border(
            top: BorderSide(width: .5, color: Colors.grey),
          )),
          child: BottomNavigationBar(
              showSelectedLabels: true,
              showUnselectedLabels: true,
              currentIndex: _currentIndex,
              onTap: (int index) {
                setState(() => _currentIndex = index);
              },
              items: [
                BottomNavigationBarItem(
                  backgroundColor: Colors.indigo[300],
                  title: Text('Info', style: TextStyle(fontSize: 15.0)),
                  icon: Icon(
                    LineAwesomeIcons.graduation_cap,
                    size: 35.0,
                  ),
                ),
                BottomNavigationBarItem(
                  backgroundColor: Colors.indigo[300],
                  title: Text('Trips', style: TextStyle(fontSize: 15.0)),
                  icon: Icon(
                    LineAwesomeIcons.train,
                    size: 35.0,
                  ),
                ),
                BottomNavigationBarItem(
                  backgroundColor: Colors.indigo[300],
                  title: Text('Settings', style: TextStyle(fontSize: 15.0)),
                  icon: Icon(
                    LineAwesomeIcons.cog,
                    size: 35.0,
                  ),
                ),
//              BottomNavigationBarItem(
//                backgroundColor: Colors.indigo[300],
//                title: Text('Insights', style: TextStyle(fontSize: 14.0)),
//                icon: Icon(
//                  LineAwesomeIcons.info,
//                  size: 30.0,
//                ),
//              ),
              ]),
        ),
      );
    }

    _buildApp() {
      return IndexedStack(
        index: _currentIndex,
        children: [
          InfoView(),
          TripsView(),
          AccountView(),
//        InsightsView(),
        ],
      );
    }

    return Scaffold(
      body: SafeArea(
        top: false,
        child: _buildApp(),
      ),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }
}
