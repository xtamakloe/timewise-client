import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:time_wise_app/models/user.dart';
import 'package:time_wise_app/provider/app_state.dart';
import 'package:time_wise_app/provider/trips_state.dart';
import 'package:time_wise_app/util/shared_prefs.dart';

enum Status { Uninitialized, Authenticated, Authenticating, NotAuthenticated }

class AuthState extends ChangeNotifier {
  User _user;
  String _authError;
  Status _status = Status.Uninitialized;

  User get user => _user;

  Status get status => _status;

  String get authError => _authError;

  Future<User> signIn(String email, String password) async {
    String _serviceUrl = '${AppState.apiEndpoint}/authenticate.json';
    final _headers = {'Content-Type': 'application/json'};

    _status = Status.Authenticating;
    notifyListeners();

    try {
      _authError = '';

      var json =
          jsonEncode({'email': email.toLowerCase(), 'password': password});
      print(_serviceUrl);
      final response =
          await http.post(_serviceUrl, headers: _headers, body: json);
      // print(response.body);

      if (response.statusCode == 200) {
        var res = jsonDecode(response.body);
        if (res['user'] != null) {
          _status = Status.Authenticated;
          notifyListeners();
          return User.fromJson(res['user']);
        }
      }

      if (response.statusCode == 401) _authError = 'Invalid login credentials';

      _status = Status.NotAuthenticated;
      notifyListeners();

      return null;
    } catch (e) {
      print('Server exception in login');
      print(e);

      _status = Status.NotAuthenticated;
      notifyListeners();
      return null;
    }
  }

  Future<void> signOut() async {
    //TODO: sign out on server
    _status = Status.NotAuthenticated;
    notifyListeners();
    return Future.delayed(Duration.zero);
  }

  Future<void> checkPreviousLogin(BuildContext context) async {
    String token = await SharedPrefs.getAuthToken();

    if (token != null) {
      _user = User(authToken: token);
      Provider.of<AppState>(context, listen: false)
          .initAppState(_user, Provider.of<TripsState>(context, listen: false));

      _user = User(authToken: token);

      _status = Status.Authenticated;
      notifyListeners();
    }
  }
}
/*
class AuthenService with ChangeNotifier {
  var currentUser;

  AuthenService() {
    print("new AuthService");
  }

  Future getUser() {
    return Future.value(currentUser);
  }

  // wrappinhg the firebase calls
  Future logout() {
    this.currentUser = null;
    notifyListeners();
    return Future.value(currentUser);
  }

  // wrapping the firebase calls
  Future createUser(
      {String firstName,
        String lastName,
        String email,
        String password}) async {}

  // logs in the user if password matches
  Future loginUser({String email, String password}) {
    if (password == 'password123') {
      this.currentUser = {'email': email};
      notifyListeners();
      return Future.value(currentUser);
    } else {
      this.currentUser = null;
      return Future.value(null);
    }
  }
}

 */
