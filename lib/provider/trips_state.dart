import 'package:flutter/material.dart';
import 'package:time_wise_app/models/trip.dart';
import 'package:time_wise_app/services/trip_service.dart';

class TripsState extends ChangeNotifier {
  List<Trip> _tripsList;

  List<Trip> get tripsList => _tripsList;

  // insert a trip into local store
  void addLocalTrip(Trip trip) {
    _tripsList.add(trip);

    // sort by departure date
    // setState(() {
    _tripsList.sort((a, b) => b.departsAt.compareTo(a.departsAt));
    notifyListeners();
    print('addLocalTrip: local trips updated');
    // });
  }

  // Updates local version of trip
  // => always make sure update successful on server first
  void updateLocalTrip(Trip trip) {
    int tripId = trip.id;
    int tripIndex = _tripsList.indexWhere((element) => element.id == tripId);
    _tripsList.replaceRange(tripIndex, tripIndex + 1, [trip]);
    notifyListeners();
    print('updateLocalTrip: local trips updated');
  }

  void removeLocalTrip(Trip trip) {
    int tripId = trip.id;
    int tripIndex = _tripsList.indexWhere((element) => element.id == tripId);
    _tripsList.removeAt(tripIndex);
    notifyListeners();
    print('removeLocalTrip: local trips updated');
  }

  // Retrieve trips from server and store locally
  Future<void> loadTripsFromServer(String authToken) async {
    List<Trip> trips = [];

    trips = await TripService.getTrips(authToken).then((responseTrips) {
      if (responseTrips != null) {
        responseTrips.sort((a, b) => b.departsAt.compareTo(a.departsAt));
        print('loadTripsFromServer() complete');
      }
      return responseTrips;
    });

    _tripsList = trips;
    notifyListeners();

    // final UpdateTripsBloc bloc = BlocProvider.of<UpdateTripsBloc>(context);
    // bloc.localTripsUpdater.add(trips);
    // print('adding trips to bloc');
  }
}
