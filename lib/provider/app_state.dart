import 'dart:io' show Platform;

import 'package:csv/csv.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:time_wise_app/models/station.dart';
import 'package:time_wise_app/models/user.dart';
import 'package:time_wise_app/provider/trips_state.dart';
import 'package:time_wise_app/util/shared_prefs.dart';

class AppState extends ChangeNotifier {
  static final String appEnv = 'development';

  static final String apiEndpoint = appEnv == 'production'
      ? 'https://timewiseapi.herokuapp.com'
      : Platform.isAndroid
          ? 'http://10.0.2.2:3000'
          : 'http://localhost:3000';

  String _authToken;
  User _user;
  List<Station> _stationsList;

  User get user => _user;

  List<Station> get stationsList => _stationsList;

  void initAppState(User user, TripsState tripsState) {
    // set token in sharedPrefs
    SharedPrefs.saveAuthToken(user.authToken);

    // set session vars
    _user = user;
    _authToken = user.authToken;

    // get stations
    loadStations();

    // load trips from server into state
    tripsState.loadTripsFromServer(_authToken);
  }

  void logout() {
    SharedPrefs.clearAuthToken();
  }

  Future<void> loadStations() async {
    List<Station> list = [];
    await rootBundle.loadString('assets/res/station_codes.csv').then((value) {
      List<List<dynamic>> rowsAsListOfValues =
          const CsvToListConverter().convert(value);
      rowsAsListOfValues.forEach((row) {
        list.add(Station(code: row[1], name: row[0]));
      });
    });
    _stationsList = list;
    print('loadStations complete');
  }
}
