import 'package:flutter/material.dart';

class TimeWiseAppBar extends StatelessWidget {
  const TimeWiseAppBar({
    Key key,
    @required this.title,
    this.actions,
  }) : super(key: key);

  final String title;
  final List<Widget> actions;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      iconTheme: IconThemeData(
        color: Colors.black, //change your color here
      ),
      bottom: PreferredSize(child: Container(color: Colors.grey[300], height: 1)),
      elevation: 0,
      backgroundColor: Colors.white,
      title: Text(this.title.toUpperCase(),
          style: TextStyle(
            fontSize: 18.0,
            color: Colors.black,
            fontWeight: FontWeight.bold,
          )),
      actions: actions,
    );
  }
}


// actions should be a list of list items (icon + text + function to run
