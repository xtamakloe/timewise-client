import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:time_wise_app/models/trip.dart';
import 'package:time_wise_app/screens/trips/trip_details_screen.dart';

class TripListItem extends StatefulWidget {
  final Trip trip;

  const TripListItem({Key key, @required this.trip}) : super(key: key);

  @override
  _TripListItemState createState() => _TripListItemState();
}

class _TripListItemState extends State<TripListItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => TripDetailsScreen(
                    trip: widget.trip,
                  )),
        );
      },
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 20.0),
        decoration: BoxDecoration(
            border: Border(
          bottom: BorderSide(width: .5, color: Colors.grey),
        )),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 2,
              child: Container(
                height: 40.0,
                width: 40.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.indigo[300],
                ),
                child: Icon(
                  widget.trip.isBusiness()
                      ? LineAwesomeIcons.briefcase
                      : LineAwesomeIcons.theater_masks,
                  size: 25.0,
                  color: Colors.white,
                ),
              ),
            ),
            Expanded(
                flex: 6,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.trip.originStationName,
                            style: TextStyle(
                                fontSize: 15.0, fontWeight: FontWeight.w500),
                          ),
                          Row(mainAxisSize: MainAxisSize.min, children: [
                            Expanded(
                              flex: 1,
                              child: Text(
                                ' to ',
                                style: TextStyle(
                                    fontSize: 15.0, color: Colors.grey),
                              ),
                            ),
                            Expanded(
                              flex: 10,
                              child: Text(
                                widget.trip.destinationStationName,
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ]),
                        ]),
//                    Text( widget.trip.title, style: TextStyle(
//                          fontSize: 16.0, fontWeight: FontWeight.w500),
//                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    if (widget.trip.purpose.isNotEmpty)
                      Text(widget.trip.purpose),
                    SizedBox(
                      height: 5.0,
                    ),
                    DefaultTextStyle.merge(
                        style: TextStyle(color: Colors.grey, fontSize: 14.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(widget.trip.date),
                            Text(widget.trip.schedule),
                          ],
                        )),
                  ],
                )),
            (widget.trip.isCompleted())
                ? Expanded(
                    flex: 1,
                    child: _tripRatingIcon(),
                  )
                : Expanded(
                    flex: 1,
                    child: Container(
                      width: 0.0,
                      height: 0.0,
                    ),
                  )
          ],
        ),
      ),
    );
  }

  Icon _tripRatingIcon() {
    switch (widget.trip.evaluationReport.tripExperienceRating) {
      case 1:
        return Icon(LineAwesomeIcons.loudly_crying_face,
            color: Colors.red[700]);
      case 2:
        return Icon(LineAwesomeIcons.frowning_face, color: Colors.red[300]);
      case 3:
        return Icon(LineAwesomeIcons.neutral_face, color: Colors.amber);
      case 4:
        return Icon(LineAwesomeIcons.smiling_face, color: Colors.green[300]);
      case 5:
        return Icon(LineAwesomeIcons.grinning_face_with_smiling_eyes,
            color: Colors.green[700]);
      default:
        return Icon(LineAwesomeIcons.neutral_face, color: Colors.amber);
    }
  }
}
