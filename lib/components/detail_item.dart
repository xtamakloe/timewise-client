import 'package:flutter/material.dart';

class DetailItemText extends StatelessWidget {
  final String detailLabel;
  final String detailValue;
  final IconData detailIcon;

  DetailItemText({this.detailLabel, this.detailValue, this.detailIcon});

  @override
  Widget build(BuildContext context) {
    return DetailItemWidget(
      detailIcon: detailIcon,
      detailLabel: detailLabel,
      detailWidget: Text(
        detailValue,
        style: TextStyle(fontWeight: FontWeight.w500),
      ),
    );
  }
}

class DetailItemWidget extends StatelessWidget {
  final String detailLabel;
  final IconData detailIcon;
  final Widget detailWidget;

  DetailItemWidget({this.detailLabel, this.detailIcon, this.detailWidget});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(flex: 1, child: Icon(detailIcon, size: 25)),
              Expanded(flex: 4, child: Text(detailLabel)),
              Expanded(flex: 2, child: Center(child: detailWidget))
            ],
          ),
          // DetailItemDivider(),
        ],
      ),
    );
  }
}

class DetailItemDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Divider(
      thickness: 1.0,
      height: 40.0,
    );
  }
}
