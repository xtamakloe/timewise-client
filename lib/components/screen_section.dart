import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class ScreenSection extends StatefulWidget {
  final String sectionTitle;
  final SectionAction sectionAction;
  final Widget sectionContent;
  final Color bgColor;

  ScreenSection({
    Key key,
    this.sectionTitle,
    this.sectionAction,
    this.sectionContent,
    this.bgColor,
  }) : super(key: key);

  @override
  _ScreenSectionState createState() => _ScreenSectionState();
}

class _ScreenSectionState extends State<ScreenSection> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 15, 0, 0),
      decoration: BoxDecoration(
        color: widget.bgColor ?? Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // header
          if (widget.sectionTitle != '') _buildSectionHeader(),
          // content

          widget.sectionContent,
        ],
      ),
    );
  }

  _buildSectionHeader() {
    return Container(
      padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            widget.sectionTitle,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 14.0,
            ),
          ),
          _buildSectionAction(),
        ],
      ),
    );
  }

  _buildSectionAction() {
    if (widget.sectionAction == null) return '';

    return InkWell(
      onTap: () async {
        if (widget.sectionAction.type == 'modal') {
          await showCupertinoModalBottomSheet(
              expand: true,
              context: context,
              backgroundColor: Colors.white,
              builder: (context, scrollController) {
                return widget.sectionAction.screen;
              }).then((value) {
            // do this when modal closes, like update the calling
            // screen or something
            widget.sectionAction.onModalClosed(value);
          });
        } else {
          Navigator.pushNamed(context, widget.sectionAction.route,
              arguments: widget.sectionAction.routeArguments);
        }
      },
      child: Text(
        widget.sectionAction.title,
        style: TextStyle(
          fontSize: 14.0,
          fontWeight: FontWeight.bold,
          color: Colors.indigo[300],
        ),
      ),
    );
  }
}

class ScreenSectionData {
  final String sectionTitle;
  final SectionAction sectionAction;
  final Widget sectionContent;
  final Color bgColor;

  ScreenSectionData(
      {this.sectionTitle,
      this.sectionAction,
      this.sectionContent,
      this.bgColor});
}

class SectionAction {
  final String title;
  final String route;
  final Map routeArguments;
  final String type;
  final Widget screen;
  final Function onModalClosed;

  SectionAction({
    this.title = '',
    this.route = '',
    this.routeArguments,
    this.type = '',
    this.screen,
    this.onModalClosed,
  });
}
