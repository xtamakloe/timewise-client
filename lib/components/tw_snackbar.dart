import 'package:flutter/material.dart';

class TWSnackBar {
  static void showAlert(BuildContext context, String type, String text) {
    Color color;

    switch (type) {
      case "success":
        color = Colors.green;
        break;
      case "error":
        color = Colors.red;
        break;
      default:
        color = Colors.grey[300];
    }

    Scaffold.of(context).showSnackBar(SnackBar(
        backgroundColor: color,
        content: Text(text,
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold))));
  }
}
