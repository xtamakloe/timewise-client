import 'package:flutter/material.dart';

class TWFlatButton extends StatelessWidget {
  final String buttonText;
  final Function onPressed;
  final bool inverted;

  const TWFlatButton({
    Key key,
    @required this.context,
    @required this.buttonText,
    @required this.onPressed,
    this.inverted,
  }) : super(key: key);

  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
//        height: 30.0,
        child: inverted
            ? OutlineButton(
                child: _buttonText(buttonText),
                onPressed: onPressed,
                textColor: Colors.indigo,
                color: Colors.white,
                borderSide: BorderSide(color: Colors.indigo[300]),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0)))
            : FlatButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0)),
                child: _buttonText(buttonText),
                textColor: Colors.white,
                color: Colors.indigo[300],
                onPressed: onPressed,
              ));
  }

  Widget _buttonText(String text) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 14,
      ),
    );
  }
}
