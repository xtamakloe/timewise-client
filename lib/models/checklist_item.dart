import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_wise_app/models/trip.dart';
import 'package:time_wise_app/provider/app_state.dart';
import 'package:time_wise_app/services/checklist_service.dart';

class APIChecklistItem {
  ChecklistItem checklistItem;

  APIChecklistItem({this.checklistItem});

  APIChecklistItem.fromJson(Map<String, dynamic> json) {
    checklistItem = json['checklist_item'] != null
        ? ChecklistItem.fromJson(json['checklist_item'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.checklistItem != null) {
      data['checklist_item'] = this.checklistItem.toJson();
    }
    return data;
  }
}

class ChecklistItem {
  int id;
  String text;
  bool complete;

  ChecklistItem({this.id, this.text, this.complete});

  // Mainly to handle results from server
  ChecklistItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    text = json['text'];
    complete = json['complete'];
  }

  // Mainly to send data to server during update, id not needed
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['text'] = this.text;
    data['complete'] = this.complete;
    return data;
  }

  update(BuildContext context, Trip trip) {
    return ChecklistService().updateChecklistItem(
        Provider.of<AppState>(context, listen: false).user.authToken,
        trip,
        this);
  }

  delete(BuildContext context, Trip trip) {
    return ChecklistService().deleteChecklistItem(
        Provider.of<AppState>(context, listen: false).user.authToken,
        trip,
        this);
  }
}
