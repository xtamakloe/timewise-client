import 'dart:core';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:time_wise_app/models/checklist_item.dart';
import 'package:time_wise_app/models/evaluation_report.dart';
import 'package:time_wise_app/models/rating_cell.dart';
import 'package:time_wise_app/services/trip_service.dart';

class APITrip {
  Trip trip;

  APITrip({this.trip});

  APITrip.fromJson(Map<String, dynamic> json) {
    trip = json['trip'] != null ? Trip.fromJson(json['trip']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.trip != null) {
      data['trip'] = this.trip.toJson();
    }
    return data;
  }
}

class Trip {
  int id;
  String originStationName;
  String originStationCode;
  String destinationStationName;
  String destinationStationCode;
  String departsAt;
  String arrivesAt;
  String duration;
  String type;
  String purpose;
  String travelDirection;
  String status;
  String rating;
  List<TripLeg> tripLegs = sampleTripLegs;
  List<RatingCell> ratingCells;
  List<ChecklistItem> checklistItems;
  String operatorName;
  String featureList;
  EvaluationReport evaluationReport;

  /*
  Trip(
      {this.id,
      this.originStationName,
      this.originStationCode,
      this.destinationStationName,
      this.destinationStationCode,
      this.departsAt,
      this.arrivesAt,
      this.type,
      this.purpose,
      this.travelDirection,
      this.rating,
      this.status,
      this.operatorName,
      this.featureList,
      this.evaluationReport}) {
//    initializeDateFormatting('de_DE', null).then(formatDates);
  }
  */

  String get title =>
      '${this.originStationName} to ${this.destinationStationName}';

  String get schedule {
    String dt = DateFormat.Hm().format(DateTime.parse(departsAt));
    String at = DateFormat.Hm().format(DateTime.parse(arrivesAt));
    return '$dt - $at';
  }

  String get date => DateFormat.yMMMMd().format(DateTime.parse(departsAt));

  bool isBusiness() => this.type == 'business';

  bool isOutbound() => this.travelDirection == 'outbound';

  bool isUpcoming() => this.status == 'upcoming';

  bool isInProgress() => this.status == 'in-progress';

  bool isCompleted() => this.status == 'completed';

  String get purposeDescription =>
      '${this.purpose} ${this.isOutbound() ? '' : '(Return)'} ';

  void start() => this.status = 'in-progress';

  void end() => this.status = 'completed';

  update(BuildContext context) {
    return TripService().updateTrip(context, this);
  }

  delete(BuildContext context) {
    return TripService().deleteTrip(context, this);
  }

  EvaluationReport getEvaluationReport() {
    if (evaluationReport == null) evaluationReport = EvaluationReport();

    return evaluationReport;
  }

  Trip.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    originStationName = json['origin_station_name'];
    originStationCode = json['origin_station_code'];
    destinationStationName = json['destination_station_name'];
    destinationStationCode = json['destination_station_code'];
    departsAt = json['departs_at'];
    arrivesAt = json['arrives_at'];
    duration = json['duration'];
    type = json['trip_type'];
    purpose = json['purpose'];
    travelDirection = json['travel_direction'];
    rating = json['rating'];
    status = json['status'];
    operatorName = json['operator_name'];
    featureList = json['feature_list'];
    if (json['rating_cells'] != null) {
      ratingCells = List<RatingCell>();
      json['rating_cells'].forEach((v) {
        ratingCells.add(RatingCell.fromJson(v));
      });
    }
    if (json['checklist_items'] != null) {
      checklistItems = List<ChecklistItem>();
      json['checklist_items'].forEach((v) {
        checklistItems.add(APIChecklistItem.fromJson(v).checklistItem);
      });
    }
    //TODO: look into APIEvaluationReport used here
    evaluationReport = json['evaluation_report'] != null
        ? EvaluationReport.fromJson(json['evaluation_report'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['origin_station_name'] = this.originStationName;
    data['origin_station_code'] = this.originStationCode;
    data['destination_station_name'] = this.destinationStationName;
    data['destination_station_code'] = this.destinationStationCode;
    data['departs_at'] = this.departsAt;
    data['arrives_at'] = this.arrivesAt;
    data['trip_type'] = this.type;
    data['purpose'] = this.purpose;
    data['travel_direction'] = this.travelDirection;
    data['rating'] = this.rating;
    data['status'] = this.status;
    if (this.evaluationReport != null)
      data['evaluation_report_attributes'] = this.evaluationReport.toJson();
    // ff will never be sent to server
    //  this.duration, this.featureList, this.ratingCells
    //  checklist items as well => they'll be updated via checklist_items endpoint

    return data;
  }

  static List<TripLeg> sampleTripLegs = [
    TripLeg(
        startStation: 'Nottingham',
        endStation: 'Beeston',
        legType: 'departure',
//        stopTime: '11:00',
        dataCells: [
          TripLegDataCell('11:00', 10, '5'),
          TripLegDataCell('11:08', 10, '5'),
        ]),
    TripLeg(
        startStation: 'Beeston',
        endStation: 'Loughborough',
        legType: 'transit',
//        stopTime: '11:09',
        dataCells: [
          TripLegDataCell('11:09', 10, '5'),
          TripLegDataCell('11:10', 10, '5'),
        ]),
    TripLeg(
        startStation: 'Loughborough',
        endStation: 'Leicester',
        legType: 'transit',
//        stopTime: '11:19',
        dataCells: [
          TripLegDataCell('11:19', 10, '4'),
          TripLegDataCell('11:27', 10, '4'),
          TripLegDataCell('11:37', 10, '3')
        ]),
    TripLeg(
        startStation: 'Leicester',
        endStation: 'Market Harborough',
        legType: 'transit',
//        stopTime: '11:38',
        dataCells: [
          TripLegDataCell('11:38', 10, '3'),
          TripLegDataCell('11:48', 10, '3'),
          TripLegDataCell('11:50', 10, '3'),
        ]),
    TripLeg(
        startStation: 'Market Harborough',
        endStation: 'Kettering',
        legType: 'transit',
//        stopTime: '11:51',
        dataCells: [
          TripLegDataCell('11:51', 10, '2'),
          TripLegDataCell('12:00', 10, '2')
        ]),
    TripLeg(
        startStation: 'Kettering',
        endStation: 'Wellingborough',
        legType: 'transit',
//        stopTime: '12:01',
        dataCells: [
          TripLegDataCell('12:01', 10, '2'),
          TripLegDataCell('12:07', 10, '2')
        ]),
    TripLeg(
        startStation: 'Wellingborough',
        endStation: 'Bedford',
        legType: 'transit',
//        stopTime: '12:08',
        dataCells: [
          TripLegDataCell('12:08', 10, '1'),
          TripLegDataCell('12:21', 10, '1')
        ]),
    TripLeg(
        startStation: 'Bedford',
        endStation: 'Luton Airport Parkway',
        legType: 'transit',
//        stopTime: '12:22',
        dataCells: [
          TripLegDataCell('12:22', 10, '0'),
          TripLegDataCell('12:32', 10, '0'),
          TripLegDataCell('12:37', 10, '0')
        ]),
    TripLeg(
        startStation: 'Luton Airport Parkway',
        endStation: 'London St Pancras',
        legType: 'arrival',
//        stopTime: '12:38',
        dataCells: [
          TripLegDataCell('12:38', 10, '1'),
          TripLegDataCell('12:48', 10, '1'),
          TripLegDataCell('12:58', 10, '1'),
          TripLegDataCell('13:08', 10, '1'),
          TripLegDataCell('13:10', 10, '1')
        ]),
  ];
}

class TripLeg {
  String startStation;
  String endStation;
  String legType;

//  String stopTime;
  List<TripLegDataCell> dataCells;

  TripLeg({
    this.startStation, // Nottingham
    this.endStation, // London St Pancras Intl
//    this.stopTime,
    this.legType, // departure, transit, arrival
    this.dataCells, // <TripLegDataCell>[]
  });
// NB: create a var legDuration => used on server side to calculate no. of cells to be
// generated
}

class TripLegDataCell {
  String timeLabel;
  int duration;
  String rating;

  TripLegDataCell(this.timeLabel, this.duration, this.rating);
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}

