import 'dart:core';

import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class RatingCell {
  int id;
  String rating;
  String stopName;
  String stopCode;
  String stopTime;

  String get stop_Time => DateFormat.Hm().format(DateTime.parse(stopTime));

  RatingCell(
      {this.id, this.rating, this.stopName, this.stopCode, this.stopTime});

  RatingCell.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    rating = json['rating'];
    stopName = json['stop_name'];
    stopCode = json['stop_code'];
    stopTime = json['stop_time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['rating'] = this.rating;
    data['stop_name'] = this.stopName;
    data['stop_code'] = this.stopCode;
    data['stop_time'] = this.stopTime;
    return data;
  }
}