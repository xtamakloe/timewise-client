class APIEvaluationReport {
  EvaluationReport evaluationReport;

  APIEvaluationReport({this.evaluationReport});

  APIEvaluationReport.fromJson(Map<String, dynamic> json) {
    evaluationReport = json['evaluation_report'] != null
        ? APIEvaluationReport.fromJson(json['evaluation_report'])
            .evaluationReport
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.evaluationReport != null) {
      data['evaluation_report'] = this.evaluationReport.toJson();
    }
    return data;
  }
}

class EvaluationReport {
  int id;
  int preTripPrepRating;
  int postTripPrepRating;
  int tripExperienceRating;
  int timeUseRating;
  String tripNotes;
  ActivityDataReport activityDataReport;
  ExperienceFactorsReport experienceFactorsReport;

  EvaluationReport();

  EvaluationReport.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    preTripPrepRating = json['pre_trip_prep_rating'];
    postTripPrepRating = json['post_trip_prep_rating'];
    tripExperienceRating = json['trip_experience_rating'];
    timeUseRating = json['time_use_rating'];
    tripNotes = json['trip_notes'];
    activityDataReport = json['activity_data_report'] != null
        ? new ActivityDataReport.fromJson(json['activity_data_report'])
        : null;
    experienceFactorsReport = json['experience_factors_report'] != null
        ? new ExperienceFactorsReport.fromJson(
            json['experience_factors_report'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['pre_trip_prep_rating'] = this.preTripPrepRating;
    data['post_trip_prep_rating'] = this.postTripPrepRating;
    data['trip_experience_rating'] = this.tripExperienceRating;
    data['time_use_rating'] = this.timeUseRating;
    data['trip_notes'] = this.tripNotes;
    if (this.activityDataReport != null) {
      data['activity_data_report_attributes'] =
          this.activityDataReport.toJson();
    }
    if (this.experienceFactorsReport != null) {
      data['experience_factors_report_attributes'] =
          this.experienceFactorsReport.toJson();
    }
    return data;
  }

  ActivityDataReport getActivityDataReport() {
    if (activityDataReport == null)
      activityDataReport = ActivityDataReport();

    return activityDataReport;
  }

  ExperienceFactorsReport getExperienceFactorsReport() {
    return experienceFactorsReport;
  }
}

class ActivityDataReport {
  String activityData; // sending to server
  String activityCodes; // retrieving from server

  ActivityDataReport({this.activityData});

  ActivityDataReport.fromJson(Map<String, dynamic> json) {
    activityCodes = json['activity_codes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['activity_data'] = this.activityData;
    return data;
  }
}

class ExperienceFactorsReport {
  int id;
  List<ExperienceFactor> experienceFactors;

  ExperienceFactorsReport();

  ExperienceFactorsReport.fromJson(Map<String, dynamic> json) {
    id = json['id'];

    if (json['experience_factors'] != null) {
      experienceFactors = List<ExperienceFactor>();
      json['experience_factors'].forEach((v) {
        experienceFactors.add(APIExperienceFactor.fromJson(v).experienceFactor);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;

    data['experience_factors_attributes'] = this.experienceFactors != null
        ? this.experienceFactors.map((i) => i.toJson()).toList()
        : null;

    return data;
  }
}

class ExperienceFactor {
  int id;
  int reportId;
  String code;
  int rating;

  ExperienceFactor({this.reportId, this.code, this.rating});

  ExperienceFactor.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    reportId = json['experience_factors_report_id'];
    code = json['code'];
    rating = json['rating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['experience_factors_report_id'] = this.reportId;
    data['code'] = this.code;
    data['rating'] = this.rating;
    return data;
  }
}

class APIExperienceFactor {
  ExperienceFactor experienceFactor;

  APIExperienceFactor();

  APIExperienceFactor.fromJson(Map<String, dynamic> json) {
    experienceFactor = json['experience_factor'] != null
        ? ExperienceFactor.fromJson(json['experience_factor'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.experienceFactor != null) {
      data['experience_factor'] = this.experienceFactor.toJson();
    }
    return data;
  }
}
