import 'package:redux/redux.dart';
import 'package:time_wise_app/models/redux/actions/set_reminder_action.dart';
import 'package:time_wise_app/models/redux/store/redux_app_state.dart';
import 'package:time_wise_app/models/redux/store/reminder_state.dart';
import 'package:time_wise_app/models/reminder.dart';

ReduxAppState appReducer(ReduxAppState state, dynamic action) => ReduxAppState(
  remindersState: remindersReducer(state.remindersState, action),
);

final remindersReducer = combineReducers<RemindersState>([
  TypedReducer<RemindersState, SetReminderAction>(_setReminderAction),
  TypedReducer<RemindersState, ClearReminderAction>(_clearReminderAction),
  TypedReducer<RemindersState, RemoveReminderAction>(_removeReminderAction),
]);

RemindersState _setReminderAction(
    RemindersState state, SetReminderAction action) {
  var remindersList = state.reminders;
  if (remindersList != null) {
    remindersList.add(new Reminder(
        repeat: action.repeat, name: action.name, time: action.time));
  }
  return state.copyWith(reminders: remindersList);
}

RemindersState _clearReminderAction(
    RemindersState state, ClearReminderAction action) =>
    state.copyWith(reminders: []);

RemindersState _removeReminderAction(
    RemindersState state, RemoveReminderAction action) {
  var remindersList = state.reminders;
  remindersList.removeWhere((reminder) => reminder.name == action.name);
  return state.copyWith(reminders: remindersList);
}