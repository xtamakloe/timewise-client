import 'package:flutter/material.dart';
import 'package:time_wise_app/models/redux/store/reminder_state.dart';

@immutable
class ReduxAppState {
  final RemindersState remindersState;

  ReduxAppState({@required this.remindersState});

  factory ReduxAppState.initial() {
    return ReduxAppState(
      remindersState: RemindersState.initial(),
    );
  }

  dynamic toJson() {
    return {'remindersState': this.remindersState.toJson()};
  }

  static ReduxAppState fromJson(dynamic json) {
    return json != null
        ? ReduxAppState(
            remindersState: RemindersState.fromJson(json['remindersState'])
    )
        : null;
  }

  ReduxAppState copyWith({RemindersState remindersState}) {
    return ReduxAppState(remindersState: remindersState ?? this.remindersState);
  }
}
