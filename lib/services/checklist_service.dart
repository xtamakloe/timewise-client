import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:time_wise_app/models/checklist_item.dart';
import 'package:time_wise_app/models/trip.dart';
import 'package:time_wise_app/provider/app_state.dart';

class ChecklistService {
  Future<ChecklistItem> createChecklistItem(
      String authToken, Trip trip, ChecklistItemFormData itemFormData) async {
    String _serviceUrl =
        '${AppState.apiEndpoint}/trips/${trip.id}/checklist_items.json';
    final _headers = {
      'Content-Type': 'application/json',
      'Authorization': authToken,
    };

    try {
      var json = itemFormData.toJson();
      final response =
          await http.post(_serviceUrl, headers: _headers, body: json);

      return APIChecklistItem.fromJson(jsonDecode(response.body)).checklistItem;
    } catch (e) {
      print('Exception in createListItem');
      print(e);
      return null;
    }
  }

  Future<ChecklistItem> updateChecklistItem(
      String authToken, Trip trip, ChecklistItem item) async {
    String _serviceUrl =
        '${AppState.apiEndpoint}/trips/${trip.id}/checklist_items/${item.id}.json';
    final _headers = {
      'Content-Type': 'application/json',
      'Authorization': authToken,
    };
    try {
      final response = await http.put(_serviceUrl,
          headers: _headers, body: jsonEncode(item.toJson()));

      return APIChecklistItem.fromJson(jsonDecode(response.body)).checklistItem;
    } catch (e) {
      print('Exception in updateChecklistItem');
      print(e);
      return null;
    }
  }

  Future<void> deleteChecklistItem(
      String authToken, Trip trip, ChecklistItem item) async {
    String _serviceUrl =
        '${AppState.apiEndpoint}/trips/${trip.id}/checklist_items/${item.id}.json';
    final _headers = {
      'Content-Type': 'application/json',
      'Authorization': authToken
    };

    try {
      final response = await http.delete(
        _serviceUrl,
        headers: _headers,
      );

      return response.statusCode == 200;
    } catch (e) {
      print('Exception in deleteChecklistItem');
      print(e);
      return null;
    }
  }
}

class ChecklistItemFormData {
  String text;

  ChecklistItemFormData({this.text});

  String toJson() {
    var mapData = new Map();
    mapData['text'] = this.text;
    String json = jsonEncode(mapData);
    return json;
  }
}
