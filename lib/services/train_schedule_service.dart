import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:time_wise_app/models/api/api_train_schedule.dart';
import 'package:time_wise_app/provider/app_state.dart';

class TrainScheduleService {
  static Future<List<APITrainSchedule>> getTrainSchedules(BuildContext context,
      String startStation, String endStation, String date, String time) async {
    AppState appState = Provider.of<AppState>(context);

    String _serviceUrl = '${AppState.apiEndpoint}/schedules.json?'
        'start_station=$startStation'
        '&end_station=$endStation'
        '&date=$date'
        '&time=$time';
    final _headers = {
      'Content-Type': 'application/json',
      'Authorization': appState.user.authToken.toString(),
    };

    try {
      final response = await http.get(_serviceUrl, headers: _headers);
      if (response.statusCode == 200) {
        return (jsonDecode(response.body) as List)
            .map((i) => APITrainSchedule.fromJson(i))
            .toList();
      }
    } catch (e) {
      print(e);
      return null;
    }
  }
}
