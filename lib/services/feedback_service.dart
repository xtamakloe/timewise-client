import 'dart:typed_data';

import 'package:http/http.dart' as http;
import 'package:time_wise_app/provider/app_state.dart';

class FeedbackService {
  static Future<bool> postFeedback(
      String authToken, String message, Uint8List imageData) async {
    String _serviceUrl = '${AppState.apiEndpoint}/feedback_posts.json';

    final _headers = {
      'Content-Type': 'application/json',
      'Authorization': authToken
    };

    try {
      var request = http.MultipartRequest('POST', Uri.parse(_serviceUrl));

      request.headers['Authorization'] = authToken;
      request.fields['feedback_post[message]'] = message;
      request.files
          .add(http.MultipartFile.fromBytes('feedback_post[image]', imageData));
      var response = await request.send();

      return (response.statusCode == 200);
    } catch (e) {
      print('Exception in postFeedback');
      print(e);
      return null;
    }
  }
}
