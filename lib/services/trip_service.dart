import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:time_wise_app/models/trip.dart';
import 'package:time_wise_app/provider/app_state.dart';

class TripService {
  static Future<List<Trip>> getTrips(String authToken) async {
    String _serviceUrl = '${AppState.apiEndpoint}/trips.json';
    final _headers = {
      'Content-Type': 'application/json',
      'Authorization': authToken,
    };
    try {
      final response = await http.get(
        _serviceUrl,
        headers: _headers,
      );

      List<APITrip> _APItrips = List<APITrip>.from(jsonDecode(response.body)
          .map((json) => APITrip.fromJson(json))
          .toList());

      List<Trip> _trips =
          List<Trip>.from(_APItrips.map((apiTrip) => apiTrip.trip)).toList();

      return _trips;
    } catch (e) {
      print('Server exception in getTrips');
      print(e);
      return null;
    }
  }

  Future<Trip> createTrip(BuildContext context, TripFormData formData) async {
    String _serviceUrl = '${AppState.apiEndpoint}/trips.json';
    AppState appState =
        Provider.of<AppState>(context, listen: false);
    final _headers = {
      'Content-Type': 'application/json',
      'Authorization': appState.user.authToken.toString(),
    };
    try {
      var json = formData.toJson();

      final response =
          await http.post(_serviceUrl, headers: _headers, body: json);

      return APITrip.fromJson(jsonDecode(response.body)).trip;
    } catch (e) {
      print('Server exception in createTrip');
      print(e);
      return null;
    }
  }

  Future<Trip> updateTrip(BuildContext context, Trip trip) async {
    AppState appState =
        Provider.of<AppState>(context, listen: false);
    String _serviceUrl =
        '${AppState.apiEndpoint}/trips/${trip.id}.json';
    final _headers = {
      'Content-Type': 'application/json',
      'Authorization': appState.user.authToken.toString(),
    };

    try {
      final response = await http.put(_serviceUrl,
          headers: _headers, body: jsonEncode(trip.toJson()));

      return APITrip.fromJson(jsonDecode(response.body)).trip;
    } catch (e) {
      print('Server exception in updateTrip');
      print(e);
      return null;
    }
  }

  Future<void> deleteTrip(BuildContext context, Trip trip) async {
    AppState appState =
        Provider.of<AppState>(context, listen: false);
    String _serviceUrl =
        '${AppState.apiEndpoint}/trips/${trip.id}.json';
    final _headers = {
      'Content-Type': 'application/json',
      'Authorization': appState.user.authToken.toString(),
    };

    try {
      final response = await http.delete(
        _serviceUrl,
        headers: _headers,
      );
      return response.statusCode == 200;
    } catch (e) {
      print('Server exception in deleteTrip');
      print(e);
      return null;
    }
  }

//
//  Future<List<RatingCell>> getTripRatings(BuildContext context, Trip trip) async {
//    var container = StateContainer.of(context);
//    String _serviceUrl = '${container.getApiUrl()}/trips/${trip.id}/ratings.json';
//    final _headers = {
//      'Content-Type': 'application/json',
//      'Authorization': container.getAppState().token.toString(),
//    };
//
//    try {
//
//      final response = await http.get(
//        _serviceUrl,
//        headers: _headers,
//      );
//
//      List<RatingCell> _ratingCells = List<RatingCell>.from(jsonDecode(response.body)
//          .map((json) => RatingCell.fromJson(json))
//          .toList());
//
//      print('getRatings complete');
//      print(_ratingCells);
//      return _ratingCells;
//
//    } catch (e) {
//      print('Server exception in updateTrip');
//      print(e);
//      return null;
//    }
//  }
//
}

class TripFormData {
  int scheduleId;
  String tripType;
  String tripPurpose;
  String travelDirection;
  String rating;

  TripFormData(
      {this.scheduleId, this.tripType, this.travelDirection, this.tripPurpose});

  String toJson() {
//    String _tripFormToJson(TripFormData formData) {
    var mapData = new Map();
    mapData['train_schedule_id'] = this.scheduleId;
    mapData['trip_type'] = this.tripType;
    mapData['purpose'] = this.tripPurpose;
    mapData['travel_direction'] = this.travelDirection;
    mapData['rating'] = this.rating;
    String json = jsonEncode(mapData);
    return json;
  }

  isValid() {
    return (this.scheduleId != null &&
        this.tripPurpose.isNotEmpty &&
        this.tripType.isNotEmpty &&
        this.travelDirection.isNotEmpty &&
        this.rating.isNotEmpty);
  }
}
