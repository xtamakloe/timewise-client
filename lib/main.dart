import 'dart:typed_data';

import 'package:feedback/feedback.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';
import 'package:redux/redux.dart';
import 'package:time_wise_app/models/redux/store/redux_app_state.dart';
import 'package:time_wise_app/models/redux/store/store.dart';
import 'package:time_wise_app/provider/app_state.dart';
import 'package:time_wise_app/provider/auth_state.dart';
import 'package:time_wise_app/provider/trips_state.dart';
import 'package:time_wise_app/screens/start_page.dart';
import 'package:time_wise_app/services/feedback_service.dart';
import 'package:time_wise_app/util/notification_helper.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();
NotificationAppLaunchDetails notificationAppLaunchDetails;
Store<ReduxAppState> store;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initStore();
  store = getStore();

  notificationAppLaunchDetails =
      await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
  //
  await initNotifications(flutterLocalNotificationsPlugin);
  requestIOSPermissions(flutterLocalNotificationsPlugin);

  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider<AuthState>(create: (_) => AuthState()),
      ChangeNotifierProvider<AppState>(create: (_) => AppState()),
      ChangeNotifierProvider<TripsState>(create: (_) => TripsState()),
    ],
    child: BetterFeedback(
        child: TimeWiseApp(store),
        drawColors: [Colors.red, Colors.blue],
        onFeedback: (BuildContext context, String feedbackText,
            Uint8List feedbackScreenshot) async {
          String authToken =
              Provider.of<AppState>(context, listen: false).user.authToken;

          FeedbackService.postFeedback(
              authToken, feedbackText, feedbackScreenshot);
          BetterFeedback.of(context).hide();
        }),
  ));
}

class TimeWiseApp extends StatefulWidget {
  final Store<ReduxAppState> store;

  TimeWiseApp(this.store);

  @override
  _TimeWiseAppState createState() => _TimeWiseAppState();
}

class _TimeWiseAppState extends State<TimeWiseApp> {
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: [
        'http://localhost:3000',
        'http://10.0.2.2:3000'
      ].contains(AppState.apiEndpoint),
      title: 'TimeWise',
      home: StartPage(),
      theme: ThemeData(
        primaryColor: Colors.indigo[300],
        unselectedWidgetColor: Colors.indigo[300],
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}
