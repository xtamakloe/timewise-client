# README
This repo hosts cross-platform mobile application for the TimeWise service. 

## TimeWise Project 
TimeWise was developed as part of a research project on how passenger personal data can be used to improve the rail travel experience. 
More information can be found [online](https://timewise-study.herokuapp.com).

##  Features 
The TimeWise platform is a service designed to help train travellers prepare for train journeys. 
Features include: 
* information on trains and travel guidance 
* a check-list for preparetion activities 
* reminders of up-coming trips 
* trip diary to record trip experiences 

The app is available for Android via the [Google Play Store](https://play.google.com/store/apps/details?id=com.artoconnect.time_wise_app) and for iOS via the App Store TestFlight service.






